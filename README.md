OpenDTG(Open Digital Twin Generator)
=====================================


Introduction
------------

[OpenDTG (Digital Twin Generator)] "Open Digital Twin Generator" is a library for generating global map for camer localization and especially targeted to the Augmented Reality community. It is designed to provide easy access to the classical problem solvers in global localization and solve them accurately.

The OpenDTG credo is: "Keep it simple, keep it maintainable". OpenDTG targets readable code that is easy to use and modify by the community.

All the features and modules are unit tested. This test-driven development ensures that the code works as it should and enables more consistent repeatability. Furthermore, it makes it easier for the user to understand and learn the given features.

[![GitHub license](https://img.shields.io/badge/license-MPL2-blue)](https://github.com/openMVG/openMVG/blob/master/LICENSE)

- [Build](#build)
- [Continuous integration](#integration)
- [Authors](#authors)
- [Contact](#contact)
- [Citations](#citations)
- [Acknowledgements](#acknowledgements)

## Build

Please follow this [build tutorial ]

## Continuous integration


## Authors

See [Authors]("available soon") text file

## Documentation

See [documentation]("available soon")

## Contact

http://arrc.kaist.ac.kr/about


## Citations


## Acknowledgements



