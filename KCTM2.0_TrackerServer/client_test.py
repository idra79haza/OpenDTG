# -*- coding: utf-8 -*-
"""
Created on Tue Jul 30 17:38:44 2019

@author: IkTiger
"""

import socket
server = '127.0.0.1' 
port = 65432
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.connect((server, port))

#msg = "{'protocol': 1, 'type': 1, 'error': 0, 'address': '127.0.0.1', 'port': '', 'message': '', 'data': {'imageAnchorPosition': {'x': 0.0, 'y': 0.0, 'z': 0.0}, 'imageAnchorRotation': {'x': 0.0, 'y': 0.0, 'z': 0.0, 'w': 0.0}, 'cameraPosition': {'x': 0.0, 'y': 0.0, 'z': 0.0}, 'cameraRotation': {'x': 0.0, 'y': 0.0, 'z': 0.0, 'w': 0.0}, 'worldPositionGPSLat': 0.0, 'worldPositionGPSLong': 0.0, 'worldPositionGPSAlt': 0.0, 'worldRotationDegrees': {'x': 0.0, 'y': 0.0, 'z': 0.0, 'w': 0.0}, 'worldRotationGyro': {'x': 0.0, 'y': 0.0, 'z': 0.0, 'w': 0.0}}}"
msg = "{\"protocol\":1,\"type\":1,\"error\":0,\"address\":\"\",\"port\":\"\",\"message\":\"\",\"data\":{\"imageAnchorPosition\":{\"x\":0.0,\"y\":0.0,\"z\":0.0},\"imageAnchorRotation\":{\"x\":0.0,\"y\":0.0,\"z\":0.0,\"w\":0.0},\"cameraPosition\":{\"x\":0.0,\"y\":0.0,\"z\":0.0},\"cameraRotation\":{\"x\":0.0,\"y\":0.0,\"z\":0.0,\"w\":0.0},\"worldPositionGPSLat\":0.0,\"worldPositionGPSLong\":0.0,\"worldPositionGPSAlt\":0.0,\"worldRotationDegrees\":{\"x\":0.0,\"y\":0.0,\"z\":0.0,\"w\":0.0},\"worldRotationGyro\":{\"x\":0.0,\"y\":0.0,\"z\":0.0,\"w\":0.0}}}"

sock.sendto(msg.encode('utf-8'), (server, port))

data, addr = sock.recvfrom(1024)
print('받은 데이터 : ', data.decode())