import socket
import json
#import keyboard
import threading
from threading import Lock

from queue import Queue
from datetime import datetime
import csv
from ast import literal_eval
import time

imageAnchorListFilepath = "ImageAnchorList01.csv"

HOST = '0.0.0.0'
PORT = 65432    # Port to listen on (non-privileged ports are > 1023)

serverSock = None


ClientInfos = {"master":{}, "tracker":{}} # 리펙토링, 구분없이, 속성만 가지도록.

ImageAnchorInfos = dict()   #"Marker GUID": Tuple (px, py, pz, qx, qy, qz, qw)
showRecivedMessage = False
aaa = 1

class ClientType:
    Master = 1
    Tracker = 2
    
class RequestType:
    RegisterClient = 1
    UpdateImageAnchorPose = 2
    UpdateCameraPose = 3
    RequestImageAnchorPose = 4
    RequestAllImageAnchorPoses = 5
    RequestAllCameraPoses = 6
    Broadcast = 7
    ToAddress = 8
    DeregisterClient = 99
    
def ReceiveData(queue):
    while True:
        try:
            recvData, addr = serverSock.recvfrom(4096)
            if showRecivedMessage is True:
                print('Data Recived from ' + str(addr) + ' : ' + str(datetime.now()))
                
        except socket.error:
            print('Soket Receive Error.')
            pass
        
        else:
            request = json.loads(recvData.decode())
            request['fromAddress'] = addr[0]
            request['port'] = addr[1]
            
            #무슨 requestType이던지 일단 dictionary에 등록한다.
            if request['clientType'] == ClientType.Master:
                ClientInfos['master'][addr[0]] = addr
                                    
            elif request['clientType'] == ClientType.Tracker:
                ClientInfos['tracker'][addr[0]] = addr
                                    
            # 삭제 요청이면 Dictionary에서 삭제.                     
            if request['requestType'] == RequestType.DeregisterClient: 
                if request['clientType'] == ClientType.Master:
                    del ClientInfos['master'][request['fromAddress']]
                   
                elif request['clientType'] == ClientType.Tracker:
                    del ClientInfos['tracker'][request['fromAddress']]
                   
            
            queue.put(request)
            
def SendData(queue):
    while True:
        request = queue.get()
        #print(request['requestType'])
        
        #RegisterClient 처리.
        if request['requestType'] == RequestType.RegisterClient:
            addr = None
            #에코로 연결 되었음을 알린다.
            if request['clientType'] == ClientType.Master:
                addr = ClientInfos['master'][request['fromAddress']];
                
            elif request['clientType'] == ClientType.Tracker:
                addr = ClientInfos['tracker'][request['fromAddress']];
                
            serverSock.sendto(json.dumps(request).encode('utf-8'), addr)
            #PrintConnectedClient()    
            
        #Broadcast 처리.
        elif request['requestType'] == RequestType.Broadcast:
            if request['clientType'] == ClientType.Master:
                #모든 Client에게 전달한다.
                for addr in ClientInfos['master'].values():
                    serverSock.sendto(json.dumps(request).encode('utf-8'), addr)
                    
                for addr in ClientInfos['tracker'].values():
                    serverSock.sendto(json.dumps(request).encode('utf-8'), addr)                
                
            if request['clientType'] == ClientType.Tracker:
                #Master 에게만 전달한다.
                for addr in ClientInfos['master'].values():
                    serverSock.sendto(json.dumps(request).encode('utf-8'), addr)                
        
        elif request['requestType'] == RequestType.UpdateImageAnchorPose:  
            response = request
            guid = request['data']['imageAnchorGUID']
            #print(guid + ' is updated')
            p = request['data']['imageAnchorPosition']
            r = request['data']['imageAnchorRotation']
            imageAnchorPose = (p['x'], p['y'], p['z'], r['x'], r['y'], r['z'], r['w'])
            date = str(datetime.now())
            value = (imageAnchorPose,  date)
                       
            ImageAnchorInfos[guid] = value
            WriteImageAnchorData(guid, value)
            #PrintImageAnchors()
            
            response['data']['imageAnchorDate'] = date;
            
            #모든 Client에게 전달한다.
            for addr in ClientInfos['master'].values():
                serverSock.sendto(json.dumps(response).encode('utf-8'), addr)
                
            for addr in ClientInfos['tracker'].values():
                serverSock.sendto(json.dumps(response).encode('utf-8'), addr)    
            
        elif request['requestType'] == RequestType.RequestImageAnchorPose:
            response = request
            guid = request['data']['imageAnchorGUID']
            targetAddr = None
            
            if request['clientType'] == ClientType.Master:
                targetAddr = ClientInfos['master'][request['fromAddress']]
                     
            if request['clientType'] == ClientType.Tracker:
                targetAddr = ClientInfos['tracker'][request['fromAddress']]
                    
            if guid in ImageAnchorInfos:
                SendImageAnchorInfo(response, guid, targetAddr)
                
            else:
               print(guid + ' is not exist in dictionary')
               #나중에 구현.
                
        elif request['requestType'] == RequestType.RequestAllImageAnchorPoses:
            response = request
            response['requestType'] = RequestType.RequestImageAnchorPose
            targetAddr = None
            
            if request['clientType'] == ClientType.Master:
                targetAddr = ClientInfos['master'][request['fromAddress']]
                     
            if request['clientType'] == ClientType.Tracker:
                targetAddr = ClientInfos['tracker'][request['fromAddress']]
                    
            for guid in ImageAnchorInfos:
                SendImageAnchorInfo(response, guid, targetAddr)
                time.sleep(1) #한번에 너무 많이보내면 받는 쪽에서 버퍼가 부족하므로 시간차를 두고 보낸다.
            
        #De-Regester 처리.
        elif request['requestType'] == RequestType.DeregisterClient : 
            #모든 Master에게 해당 Client가 나감을 알린다.
            
            for addr in ClientInfos['master'].values():
                serverSock.sendto(json.dumps(request).encode('utf-8'), addr)
            PrintConnectedClient()      
            
        #print('Send Data : {}'.format(request))
        
        queue.task_done()
 
    
def SendImageAnchorInfo(response, guid, targetAddr):
    imageAnchorPose, date = ImageAnchorInfos[guid]
    response['data']['imageAnchorGUID'] = guid
    response['data']['imageAnchorPosition']['x'] = imageAnchorPose[0]
    response['data']['imageAnchorPosition']['y'] = imageAnchorPose[1]
    response['data']['imageAnchorPosition']['z'] = imageAnchorPose[2]
    response['data']['imageAnchorRotation']['x'] = imageAnchorPose[3]
    response['data']['imageAnchorRotation']['y'] = imageAnchorPose[4]
    response['data']['imageAnchorRotation']['z'] = imageAnchorPose[5]
    response['data']['imageAnchorRotation']['w'] = imageAnchorPose[6]
    response['data']['imageAnchorDate'] = date;
      
    serverSock.sendto(json.dumps(response).encode('utf-8'), targetAddr)

def PrintConnectedClient():
    print ('#######################')
    print ('Master Client Count : ' + str(len(ClientInfos['master'])))  
    for idx, (key, value) in enumerate(ClientInfos['master'].items()):
        print('#' + str(idx) + '. ' + key + '\n')
        
    print ('Tracker Client Count : ' + str(len(ClientInfos['tracker'])))
    for idx, (key, value) in enumerate(ClientInfos['tracker'].items()):
        print('#' + str(idx) + '. ' + key + '\n')
        
    print ('#######################')        
  
def PrintImageAnchors():
    print ('#######################')
    print ('ImageAnchor Count : ' + str(len(ImageAnchorInfos)))  
    for idx,(key, value) in enumerate(ImageAnchorInfos.items()):
         print('#' + str(idx) + '. ' + key +' : '+ str(value) + '\n')
    print ('#######################')
      

def WriteImageAnchorData(guid, data):
    csv_columns = ['guid','data']

    try:
        with open(imageAnchorListFilepath, 'w') as csvfile:
               w = csv.DictWriter(csvfile, delimiter=',', lineterminator='\n',fieldnames=csv_columns)
               data = [dict(zip(csv_columns, [k, v])) for k, v in ImageAnchorInfos.items()]
               w.writerows(data)
               
    except IOError:
        print("I/O error") 
        
def LoadImageAnchorData():
    try:
        with open(imageAnchorListFilepath, mode='r') as infile:
            reader = csv.reader(infile)
            for rows in reader:
                ImageAnchorInfos[rows[0]] = literal_eval(rows[1]) #func'literal_eval' convert string to tuple.

    except IOError:
        print("I/O error") 
    
def ShowCommandOptions():
    
    print ('#######################')
    print('0. Exit')
    print('1. Show all clients.')
    print('2. Show all image anchors.')
    print('3. Disconnect the client by force.')
    print('4. Remove the client.')
    print('5. Enble/Disable to show recived messages.')
    
    selOpt = int(input("Wait for command : "))
    print ('#######################')
    
    return selOpt

def Command():
    while True:
        try:
            selOpt = ShowCommandOptions()
            
        except ValueError:
            print("Value Error")
            continue
        else:
            if selOpt is 1:
                PrintConnectedClient()
            
            elif selOpt is 2:
                PrintImageAnchors()
            elif selOpt is 3:
                sel = int(input("Select the index to disconnect : "))
                
            elif selOpt is 4:
                sel = int(input("Select the index to remove : "))
                
            elif selOpt is 5:
                global showRecivedMessage               
                showRecivedMessage = not showRecivedMessage
                print("Show message state : " + str(showRecivedMessage))      

if __name__ == '__main__':
    LoadImageAnchorData()
    #PrintImageAnchors();
    
    queue = Queue()
          
    serverSock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    serverSock.bind((HOST, PORT))
    #serverSock.listen(5)
    
    print("Server on")
    
    thread_receive = threading.Thread(target=ReceiveData, args=(queue,))
    thread_send = threading.Thread(target=SendData, args=(queue,))
    thread_command = threading.Thread(target=Command)
        
    thread_receive.daemon = True;
    thread_send.daemon = True;
    thread_command.daemon = True;
    
    thread_receive.start()
    thread_send.start()
    thread_command.start()
    
    thread_receive.join()
    thread_send.join()
    thread_command.join()
    
    queue.join()
 