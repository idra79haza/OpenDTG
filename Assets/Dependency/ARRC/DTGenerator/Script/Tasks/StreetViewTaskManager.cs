﻿using ARRC.Commons;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace ARRC.DigitalTwinGenerator
{
    public class StreetViewTaskManager : TaskManager
    {
        static StreetViewTaskManager instance;
        public static StreetViewTaskManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new StreetViewTaskManager();

                return instance;
            }
        }
        StreetViewGenerator svGen = new StreetViewGenerator();
        DownloadManager downloadManager = new DownloadManager();
        // Start is called before the first frame update
        public void Initialize(StreetViewItemMono coroutinParent, List<GameObject> goList, TextureType textureType, int targetZoom = -1)
        {
            base.Initialize();

            var svInfoList = from go in goList
                             select go.GetComponent<StreetViewItemMono>().streetviewInfo;

            bool reset = false;
            if (!reset)
            {
                //string resultFolder = "berlin";
                //AddTask(new CoroutineMonoTask("Making a download list of metadata ...", coroutinParent, svGen, svGen.AddDownloadItemsForMetaData_fromPanoIDList(resultFolder, panoid.ToArray(), downloadManager)));
                //AddTask(new DownloadTask("Downloading list of the metadata ...", downloadManager, 8));
                //AddTask(new CoroutineMonoTask("Making a streetviewinfo list of metadata ...", coroutinParent, svGen, svGen.AddStreetViewInfoFromFilename(svInfoList, resultFolder, panoid.ToArray())));
                //AddTask(new CoroutineMonoTask("Reset streetviewinfo ...", coroutinParent, svGen, svGen.ResetStreetViewObject(goList, svInfoList)));

                if (textureType == TextureType.Color)
                {
                    //svGen.imgObjList = goList;
                    //코루틴 시 첫번재 object의 update를 따름.
                    AddTask(new CoroutineMonoTask("Making a download list of image fragments ...", coroutinParent, svGen, svGen.AddDownloadItemsForPartImages(svInfoList.ToList(), downloadManager, targetZoom)));
                    AddTask(new DownloadTask("Downloading the image fragments ...", downloadManager, 4));
                    AddTask(new CoroutineMonoTask("Importing images after merging fragments into one ...", coroutinParent, svGen, svGen.WriteStreetViewColors(svInfoList.ToList(), targetZoom)));
                    AddTask(new CoroutineMonoTask("Loading the images into gameobjects...", coroutinParent, svGen, svGen.LoadTextures(goList, TextureType.Color, targetZoom)));
                }

                else if (textureType == TextureType.Depth)
                {
                    AddTask(new CoroutineMonoTask("Converting and Importing depth data ...", coroutinParent, svGen, svGen.WriteStreetViewDepths(svInfoList.ToList(), 255, true, true, true)));
                    AddTask(new CoroutineMonoTask("Loading the images into gameobjects...", coroutinParent, svGen, svGen.LoadTextures(goList, TextureType.Depth)));
                }
                else if (textureType == TextureType.Label)
                {
                    AddTask(new CoroutineMonoTask("Loading the images into gameobjects...", coroutinParent, svGen, svGen.LoadTextures(goList, TextureType.Label)));
                }


            }

        }
    }
}

