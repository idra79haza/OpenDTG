﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Xml;
using System.IO;
using System;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    public class DownloadItem_StreetViewInfo : DownloadItem
    {
        public UnityWebRequest uwr;
        public Dictionary<string, string> headers;

        public DownloadItem_StreetViewInfo(string url) : this(UnityWebRequest.Get(url))
        {
        }

        public DownloadItem_StreetViewInfo(UnityWebRequest uwr)
        {
            this.uwr = uwr;
        }

        public override float Progress
        {
            get { return uwr.downloadProgress; }
        }
        public override bool Exists
        {
            get { return false; }
        }
        public override void CheckComplete()
        {
            if (!uwr.isDone) return;

            if (string.IsNullOrEmpty(uwr.error))
            {
                string text = uwr.downloadHandler.text;

                SaveSVInfoData(text);
                //svdata = new StreetViewItem(lat, lon, image_date, elevation);
                //byte[] bytes = uwr.downloadHandler.data;
                //SaveWWWData(bytes);
                //DispatchCompete(ref bytes);
            }
            else Debug.LogWarning("Download failed: " + uwr.url + "\n" + uwr.error);

            downloadManager.completeSize += averageSize;
            complete = true;

            Dispose();
        }

        public override void Dispose()
        {
            base.Dispose();

            uwr.Dispose();
            uwr = null;
        }

        public override void Start()
        {
            if (headers != null)
            {
                foreach (var header in headers) uwr.SetRequestHeader(header.Key, header.Value);
            }
#if UNITY_2018_2_OR_NEWER
            uwr.SendWebRequest();
#else
            uwr.Send();
#endif
        }

        void SaveSVInfoData(string text_xml)
        {
            

            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(text_xml);

                try
                {
                    XmlNode fnode = doc.SelectSingleNode("panorama/data_properties");
                    string panoid = XMLExt.GetAttribute<string>(fnode, "pano_id");
                    string filepath = Path.Combine(directory, panoid + ".xml");

                    if (!Directory.Exists(directory)) Directory.CreateDirectory(directory);

                    if (!File.Exists(filepath))
                    {
                        File.WriteAllText(filepath, text_xml);
                    }
                }
                catch
                {
                    //    throw new Exception("The request is not validate for data properties.");
                    //throw;
                }

                //DictionaryOfStringAndString metadata = new DictionaryOfStringAndString();
                //foreach (string property in StreetViewItem.properties_data)
                //    metadata.Add(property, XMLExt.GetAttribute<string>(fnode, property));

                ////projection property.
                //XmlNode fnode = doc.SelectSingleNode("panorama/projection_properties");
                //if (fnode == null)
                //{
                //    throw new Exception("The reques is not validate for data projection properties.");
                //    //return;
                //}

                //foreach (string property in StreetViewItem.properties_projection)
                //    metadata.Add(property, XMLExt.GetAttribute<string>(fnode, property));

                ////text property.
                //fnode = doc.SelectSingleNode("panorama/data_properties/text");
                //if (fnode == null)
                //    metadata.Add("text", "");
                //else
                //    metadata.Add("text", fnode.InnerText);

                ////region property.
                //fnode = doc.SelectSingleNode("panorama/data_properties/region");
                //if (fnode == null)
                //    metadata.Add("region", "");
                //else
                //    metadata.Add("region",  fnode.InnerText);

                ////country
                //fnode = doc.SelectSingleNode("panorama/data_properties/country");
                //if (fnode == null)
                //    metadata.Add("country", "");
                //else
                //    metadata.Add("country", fnode.InnerText);

                ////depth property.
                //fnode = doc.SelectSingleNode("panorama/model/depth_map");
                //if (fnode == null)
                //    metadata.Add("depth_map", "");

                //else
                //    metadata.Add("depth_map", fnode.InnerText);

                ////쓰기.
                //using (StreamWriter outputFile = new StreamWriter(filename, true))
                //{
                //    // string content = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", pano_id, image_date, lat, lon, origianl_lat, original_lng, elevation_wgs84_m, pano_yaw_deg, tilt_yaw_deg, tilt_pitch_deg, depth);
                //    string content = "";
                //    foreach (KeyValuePair<string, string> data in metadata)
                //    {
                //        string line = data.Key + ":" + data.Value;
                //        content += line + "$$";
                //    }
                //    outputFile.WriteLine(content);
                //}
            }
            catch (Exception e)
            {
                //위 규칙으로 parsing 안되는 경우 pass.
                Debug.LogError(e);
                return;
            }

        }
    }
}
