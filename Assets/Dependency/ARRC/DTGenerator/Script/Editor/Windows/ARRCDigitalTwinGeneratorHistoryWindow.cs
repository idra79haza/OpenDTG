﻿using ARRC.Commons;
using ARRC.DigitalTwinGenerator;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using UnityEditor;
using UnityEngine;

namespace ARRC.DigitalTwinGenerator
{
    public class ARRCDigitalTwinGeneratorHistoryWindow : EditorWindow
    {
        private const int maxRecentCount = 5;

        private static List<ARRCXMLItem> recent = new List<ARRCXMLItem>();
        private Vector2 scrollPosition;

        private void OnGUI()
        {
            GUIStyle headerStyle = new GUIStyle(EditorStyles.label);
            headerStyle.alignment = TextAnchor.MiddleCenter;

            EditorGUILayout.BeginHorizontal(EditorStyles.toolbar);
            GUILayout.Label("№", headerStyle, GUILayout.Width(40));
            GUILayout.Label("Title", headerStyle);
            GUILayout.Label("Time", headerStyle, GUILayout.Width(120));
            GUILayout.Box("", GUIStyle.none, GUILayout.Width(50));
            EditorGUILayout.EndHorizontal();

            scrollPosition = EditorGUILayout.BeginScrollView(scrollPosition);
            GUIContent useContent = new GUIContent(">", "Restore");
            GUIContent deleteContent = new GUIContent("X", "Remove");

            int removeIdx = -1;
            int openIdx = -1;
            for (int i = 0; i < recent.Count; i++)
            {
                ARRCXMLItem item = recent[i];
                EditorGUILayout.BeginHorizontal();
                GUILayout.Label((i+1).ToString(), GUILayout.Width(40));
                GUILayout.Label(((DTConfigurationItem) item).title);

                DateTime time = new DateTime(item.timestamp);

                GUILayout.Label(time.ToString("yyyy-MM-dd HH:mm"), GUILayout.Width(120));

                if (GUILayout.Button(useContent, GUILayout.Width(20))) openIdx = i;

                if (GUILayout.Button(deleteContent, GUILayout.Width(20))) removeIdx = i;

                EditorGUILayout.EndHorizontal();

            }
            if(removeIdx != -1)
            {
                recent.RemoveAt(removeIdx);
                Save();
            }
            if(openIdx != -1)
            {
                ARRCDigitalTwinGeneratorWindow.Instance.config_UI = ((DTConfigurationItem)recent[openIdx]).DeepCopy();
                ARRCDigitalTwinGeneratorWindow.Instance.Repaint();
            }


            EditorGUILayout.EndScrollView();
        }
        private void OnEnable()
        {
            Load();
        }
        public static void Load()
        {
            //recent = new List<ARRCXMLItem>();
            //else recent.Clear();
            recent.Clear();
            if (!File.Exists(ARRCPaths.HistoryFilePath)) return;

            XmlDocument doc = new XmlDocument();
            doc.Load(ARRCPaths.HistoryFilePath);

            foreach (XmlNode node in doc.FirstChild.ChildNodes)
                recent.Add(new DTConfigurationItem(node));
        }

        //public static void Add(ARRCXMLItem prefs)
        //{
        //    string id = Guid.NewGuid().ToString();
        //    string path = Path.Combine(historyCacheFolder, id + ".xml");
        //    File.WriteAllText(path, prefs.ToXML(new XmlDocument()).OuterXml, Encoding.UTF8);

        //    if (recent == null) Load();
        //    recent.Add(new ARRCXMLItem(prefs, id));

        //    Save();
        //}
        private static void Save()
        {
            XmlDocument doc = new XmlDocument();
            XmlNode historyNode = doc.CreateElement("History");
            foreach (ARRCXMLItem item in recent)
            {
                XmlNode configNode = doc.CreateElement("Configuration");
                historyNode.AppendChild(configNode);
                item.AppendDataToNode(configNode);
            }
            doc.AppendChild(historyNode);
            File.WriteAllText(ARRCPaths.HistoryFilePath, doc.OuterXml, Encoding.UTF8);
        }

        //[MenuItem("ARRC/History")]
        public static void OpenWindow()
        {
            GetWindow<ARRCDigitalTwinGeneratorHistoryWindow>(false, "History");
        }
    }
}
