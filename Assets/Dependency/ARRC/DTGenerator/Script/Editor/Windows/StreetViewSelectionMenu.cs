﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ARRC.DigitalTwinGenerator
{
    public class StreetViewSelectionMenu : EditorWindow
    {

        [MenuItem("Tools/*Select only streetviews*")]
        static void SelectStreetViewObjects()
        {

            var filteredList = new List<GameObject>();

            if (Selection.gameObjects.Length > 0)
            {
                foreach (GameObject candidate in Selection.gameObjects)
                {
                    var compo = candidate.transform.GetComponent<StreetViewItemMono>();
                    if (compo != null)
                        filteredList.Add(candidate);
                }
            }
            Selection.objects = filteredList.ToArray();
        }
    }
}