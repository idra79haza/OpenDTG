﻿using ARRC.Commons;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ARRC.DigitalTwinGenerator
{
    class ARRCDigitalTwinGeneratorSettingWindow : EditorWindow
    {

        Dictionary<ARRCPrefPathID, bool> showMenu = new Dictionary<ARRCPrefPathID, bool>();
        Dictionary<ARRCPrefPathID, bool> useDefualtFolder = new Dictionary<ARRCPrefPathID, bool>();
        Dictionary<ARRCPrefPathID, string> customFolder = new Dictionary<ARRCPrefPathID, string>();

        void OnGUI()
        {
            foreach (ARRCPrefPathID id in Enum.GetValues(typeof(ARRCPrefPathID)).Cast<ARRCPrefPathID>())
                SelectFolderGUI(id);

            EditorGUILayout.BeginHorizontal();
            
            if (GUILayout.Button("Save"))
            {
                foreach (ARRCPrefPathID id in Enum.GetValues(typeof(ARRCPrefPathID)).Cast<ARRCPrefPathID>())
                {
                    if (!useDefualtFolder[id]) ARRCPreference.SetPreference(id, customFolder[id]); 
                    else ARRCPreference.SetPreference(id, ARRCPreference.defaultFolder[id]);
                }
                Close();
            }

            if (GUILayout.Button("Revert to default settings", GUILayout.ExpandWidth(false)))
            {
                int result = EditorUtility.DisplayDialogComplex("Revert to default settings", "Reset generation settings?", "Reset", "Ignore", "Cancel");

                if (result == 0)
                {
                    //파일 삭제
                    //if (File.Exists(ARRCPreference.prefsFilename)) File.Delete(ARRCPreference.prefsFilename);

                    ARRCPreference.DeleteAllPrefence();
                    InitMembers();
                }
            }
            EditorGUILayout.EndHorizontal();
        }

        private void OnEnable()
        {
            InitMembers();
        }
        void InitMembers()
        {
            showMenu.Clear();
            useDefualtFolder.Clear();
            customFolder.Clear();

            foreach (ARRCPrefPathID id in Enum.GetValues(typeof(ARRCPrefPathID)).Cast<ARRCPrefPathID>())
            {
                showMenu[id] = true;
                customFolder[id] = ARRCPreference.LoadPreference(id);

                string folderPath = ARRCPreference.LoadPreference(id);
                if (folderPath == ARRCPreference.defaultFolder[id])
                    useDefualtFolder[id] = true;
                else
                    useDefualtFolder[id] = false;
            }
        }
        private void SelectFolderGUI(ARRCPrefPathID id)
        {
            EditorGUILayout.BeginVertical(GUI.skin.box);
            showMenu[id] = EditorGUILayout.Foldout(showMenu[id] , id.ToString() + " Setting");
            if(showMenu[id])
            {
                
                useDefualtFolder[id] = GUILayout.Toggle(useDefualtFolder[id], ARRCPreference.defaultFolder[id]);
                useDefualtFolder[id] = !GUILayout.Toggle(!useDefualtFolder[id], "Custom folder");

                if (!useDefualtFolder[id])
                {
                    GUILayout.BeginHorizontal();
                    customFolder[id] = EditorGUILayout.TextField("", customFolder[id]);
                    GUI.SetNextControlName("BrowseButton");
                    if (GUILayout.Button("Browse", GUILayout.ExpandWidth(false)))
                    {
                        GUI.FocusControl("BrowseButton");
                        string newCustomFolder = EditorUtility.OpenFolderPanel("Select the folder.", ARRCPreference.LoadPreference(id), "");
                        if (!string.IsNullOrEmpty(newCustomFolder)) customFolder[id] = newCustomFolder;
                    }
                    GUILayout.EndHorizontal();
                }
                
            }
            EditorGUILayout.EndVertical();

            GUILayout.Space(10);

        }
        private static ARRCDigitalTwinGeneratorSettingWindow wnd;

        //[MenuItem("ARRC/Directory Setting")]
        public static void OpenWindow()
        {
            wnd = GetWindow<ARRCDigitalTwinGeneratorSettingWindow>(false, "ARRC Preference Setting");
        }
        private void OnDestroy()
        {
            wnd = null;
        }


    }
}
