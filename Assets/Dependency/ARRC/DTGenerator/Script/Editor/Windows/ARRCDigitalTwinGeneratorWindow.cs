﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using System.Xml;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    public class ARRCDigitalTwinGeneratorWindow : EditorWindow
    {
        DTGenTaskManager taskManager = DTGenTaskManager.Instance;

        //GUI
        public DTConfigurationItem config_UI = new DTConfigurationItem();
        Vector2 scrollPos = Vector2.zero;
        bool showCoordinateMenu = true;
        bool showOutputCoordinateMenu = false;

        CoordinateInfo coordConvertor_input = new CoordinateInfo();
        CoordinateInfo coordConvertor_output = new CoordinateInfo();

      

        void OnGUI()
        {
            if (taskManager.IsEmpty)
            {
                Toolbar();
                scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

                config_UI.title = EditorGUILayout.TextField("Title", config_UI.title);
                string path_data = ARRCPaths.GetResultFolder_Root(config_UI.title);
                string path_cache = ARRCPreference.LoadPreference(ARRCPrefPathID.CacheFolder);

                EditorGUILayout.LabelField("Result Folder : " + path_data, EditorStyles.boldLabel);
                EditorGUILayout.LabelField("Cache Folder : " + path_cache, EditorStyles.boldLabel);

                GUILayout.Space(10);
                EditorGUILayout.BeginVertical(GUI.skin.box);
                showCoordinateMenu = EditorGUILayout.Foldout(showCoordinateMenu, "Input coordinates");
                if (showCoordinateMenu) InputCoordinatesGUI();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                showOutputCoordinateMenu = EditorGUILayout.Foldout(showOutputCoordinateMenu, "Output coordinates");
                if (showOutputCoordinateMenu) OutputCoordinatesGUI();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.BeginHorizontal();
                config_UI.generateStreetViews = GUILayout.Toggle(config_UI.generateStreetViews, "Generate Streetviews");
                EditorGUILayout.EndHorizontal();
                if (config_UI.generateStreetViews) StreetViewGUI();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.BeginHorizontal();
                config_UI.generateBuildings = GUILayout.Toggle(config_UI.generateBuildings, "Generate Buildings");
                EditorGUILayout.EndHorizontal();
                if (config_UI.generateBuildings) BuildingGUI();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                EditorGUILayout.BeginHorizontal();
                config_UI.generatePointCloud = GUILayout.Toggle(config_UI.generatePointCloud, "Generate Point Cloud");
                EditorGUILayout.EndHorizontal();
                if (config_UI.generatePointCloud) PointCloudGUI();
                EditorGUILayout.EndVertical();

                

                

                GUILayout.EndScrollView();

                GUILayout.BeginHorizontal();
                if (GUILayout.Button("Start"))
                    StartCapture();

                GUILayout.EndHorizontal();

            }
            else
                OnGenerate();
        }

        void Update()
        {
            taskManager.Update();
        }

        void OnDestroy()
        {
            taskManager.Dispose();
            instance = null;
        }

        void StartCapture()
        {
            // check validiation.
            if (config_UI.title == "")
            {
                Debug.LogError("The title is empty.");
                return;
            }
            if (!CheckCoordValidation((config_UI.inputRange)))
            {
                Debug.LogError("The input coordinates are invalid.");
                return;
            }
            DTConfigurationItem config_param = config_UI;
            
            //Task를 구성.
            taskManager = new DTGenTaskManager();
            taskManager.Initialize(config_param, Instance);
        }

        void OnGenerate()
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);

            int phaseCount = taskManager.TaskCount;
            float progress = 0;

            for (int i = 0; i < phaseCount; i++)
            {
                if (i < taskManager.ActiveIndex)
                    progress = 1;

                else if (i == taskManager.ActiveIndex)
                    progress = taskManager.ProgressOfActiveTask;

                else if (i > taskManager.ActiveIndex)
                    progress = 0;

                float totalSize = taskManager.TotalSizeOf(i);
                int completedSize = Mathf.FloorToInt(totalSize * progress);
                string phaseState = taskManager.StateOf(i);
                TaskType taskType = taskManager.TaskTypeOf(i);

                GUILayout.Label(phaseState);
                string strProgress = Mathf.FloorToInt(progress * 100f).ToString() + "%";

                if (taskType == TaskType.Download)
                    strProgress += " (" + completedSize + " of " + totalSize + " mb)";

                Rect r = EditorGUILayout.BeginVertical();
                EditorGUI.ProgressBar(r, progress, strProgress);
                GUILayout.Space(16);
                EditorGUILayout.EndVertical();
            }
            GUILayout.Space(16);
            GUILayout.EndScrollView();

            if (!taskManager.IsCompleted && GUILayout.Button("Cancel"))
            {
                taskManager.Dispose();
            }

            if (taskManager.IsCompleted && GUILayout.Button("Done"))
            {
                //현재 설정을 저장.
                config_UI.timestamp = DateTime.Now.Ticks;
                config_UI.AppendToXML(ARRCPaths.HistoryFilePath);
                taskManager.Dispose();
            }

            GUILayout.Label("Warning: Keep this window open.");
            Repaint();
        }

        void InputCoordinatesGUI()
        {
            int prevInputCRSIndex = config_UI.inputCRSIndex;
            
            config_UI.inputCRSIndex = EditorGUILayout.Popup("Select CRS", config_UI.inputCRSIndex, CoordinateDB.CRSKeys); ;

            if(prevInputCRSIndex != config_UI.inputCRSIndex)
            {
                ResetConverterByInputChange(prevInputCRSIndex);
                config_UI.inputRange = coordConvertor_input.ConvertRange(config_UI.inputRange);
            }

            (double minX, double minY, double maxX, double maxY) newInputRange;
        
            GUILayout.Label("Bottom-Left (Origin)");
            EditorGUI.indentLevel++;
            newInputRange.minX = EditorGUILayout.DoubleField("Min X", config_UI.inputRange.minX);
            newInputRange.minY = EditorGUILayout.DoubleField("Min Y", config_UI.inputRange.minY);
            EditorGUI.indentLevel--;
            GUILayout.Space(10);

            GUILayout.Label("Top-Right");
            EditorGUI.indentLevel++;
            newInputRange.maxX = EditorGUILayout.DoubleField("Max X", config_UI.inputRange.maxX);
            newInputRange.maxY = EditorGUILayout.DoubleField("Max Y", config_UI.inputRange.maxY);

            EditorGUI.indentLevel--;
            GUILayout.Space(10);
            config_UI.inputRange = newInputRange;

            GUI.SetNextControlName("InsertCoordsButton");
            if (GUILayout.Button("Run the helper")) RunHelper();
            if (GUILayout.Button("Insert the coordinates from the clipboard")) InsertCoordsFromClipBoard();
            GUILayout.Space(10);
        }

        void OutputCoordinatesGUI()
        {
            int prevOutputCRSIndex = config_UI.outputCRSIndex;
            config_UI.outputCRSIndex = EditorGUILayout.Popup("Select CRS", config_UI.outputCRSIndex, CoordinateDB.CRSKeys); ;

            
            if (prevOutputCRSIndex != config_UI.outputCRSIndex)
                ResetConverterByOutputChanged();

            if(coordConvertor_output.Init)
                config_UI.outputRange = coordConvertor_output.ConvertRange(config_UI.inputRange);

            EditorGUILayout.LabelField("Min X", config_UI.outputRange.minX.ToString());
            EditorGUILayout.LabelField("Min Y", config_UI.outputRange.minY.ToString());
            GUILayout.Space(10);

            EditorGUILayout.LabelField("Max X", config_UI.outputRange.maxX.ToString());
            EditorGUILayout.LabelField("Max Y", config_UI.outputRange.maxY.ToString());
            GUILayout.Space(10);
        }

        void ResetConverterByInputChange(int prevInputCRSIndex)
        {
            string originalCRSName_input = CoordinateDB.GetCRSCode(prevInputCRSIndex);
            string convertedCRSName_input = CoordinateDB.GetCRSCode(config_UI.inputCRSIndex);
            string convertedCRSName_output = CoordinateDB.GetCRSCode(config_UI.outputCRSIndex);

            coordConvertor_input.InitConverter(originalCRSName_input, convertedCRSName_input);
            coordConvertor_output.InitConverter(convertedCRSName_input, convertedCRSName_output);
            
        }
        void ResetConverterByOutputChanged()
        {
            string inputCRSName = CoordinateDB.GetCRSCode(config_UI.inputCRSIndex);
            string outputCRSName = CoordinateDB.GetCRSCode(config_UI.outputCRSIndex);

            coordConvertor_output.InitConverter(inputCRSName, outputCRSName);
        }

        void RunHelper()
        {
            //
            string WGS84Code = CoordinateDB.GetCRSCode("WGS84");
            string inputCRSCode = CoordinateDB.GetCRSCode(config_UI.inputCRSIndex);
            (double minLon, double minLat, double maxLon, double maxLat) inputRange_wgs84 = config_UI.inputRange;

            // source-> wgs84 으로 좌표계 변환하여 입력 영역 구하기.
            if (inputCRSCode != WGS84Code)
            {
                CoordinateInfo coordConvertor_sourceToWgs84 = new CoordinateInfo();
                coordConvertor_sourceToWgs84.InitConverter(inputCRSCode, WGS84Code);
                inputRange_wgs84 = coordConvertor_sourceToWgs84.ConvertRange(config_UI.inputRange);
            }

            //
            string coordPath = Directory.GetFiles(Application.dataPath, "ARRC_Coords.jscript", SearchOption.AllDirectories)[0].Replace('\\', '/');
            string coordsScript = string.Format("var Coords = {{tlx: {0}, tly: {1}, brx: {2}, bry: {3}}}",
                inputRange_wgs84.minLon.ToString(), inputRange_wgs84.minLat.ToString(), inputRange_wgs84.maxLon.ToString(), inputRange_wgs84.maxLat.ToString());

            File.WriteAllText(coordPath, coordsScript);

            string helperPath = "file://" + Directory.GetFiles(Application.dataPath, "Helper_StreetView.html", SearchOption.AllDirectories)[0].Replace('\\', '/');
            if (Application.platform == RuntimePlatform.OSXEditor) helperPath = helperPath.Replace(" ", "%20");
            Application.OpenURL(helperPath);
        }
        
        public void InsertCoordsFromClipBoard()
        {
            GUI.FocusControl("InsertCoordsButton");
            string nodeStr = EditorGUIUtility.systemCopyBuffer;
            if (string.IsNullOrEmpty(nodeStr)) return;

            XmlDocument doc = new XmlDocument();

            try
            {
                doc.LoadXml(nodeStr);
                XmlNode fnode = doc.FirstChild;
                if (fnode.Name == "Coords" && fnode.Attributes != null)
                {
                    (double minX, double minY, double maxX, double maxY) newInputRange;
                    newInputRange.minX = XMLExt.GetAttribute<float>(fnode, "tlx");
                    newInputRange.minY = XMLExt.GetAttribute<float>(fnode, "bry");
                    newInputRange.maxX = XMLExt.GetAttribute<float>(fnode, "brx");
                    newInputRange.maxY = XMLExt.GetAttribute<float>(fnode, "tly");
                    config_UI.inputCRSIndex = 0;
                    config_UI.inputRange = newInputRange;
                }
            }

            catch { throw; }
        }
     
        void Toolbar()
        {
            GUIStyle buttonStyle = new GUIStyle(EditorStyles.toolbarButton);

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("History", buttonStyle, GUILayout.ExpandWidth(false)))
            {
                ARRCDigitalTwinGeneratorHistoryWindow.OpenWindow();
            }

            // if (RealWorldTerrainUpdaterWindow.hasNewVersion)
            {
                //Color defColor = GUI.backgroundColor;
                //GUI.backgroundColor = new Color(1, 0.5f, 0.5f);
                //if (GUILayout.Button("New version available!!! Click here to update.", buttonStyle))
                //{
                //    wnd.Close();
                //    RealWorldTerrainUpdaterWindow.OpenWindow();
                //}
                //GUI.backgroundColor = defColor;
            }
            //else
            GUILayout.Label("", buttonStyle);

            if (GUILayout.Button("Settings", buttonStyle, GUILayout.ExpandWidth(false)))
            {
                ARRCDigitalTwinGeneratorSettingWindow.OpenWindow();
            }

            GUILayout.EndHorizontal();

            EditorGUILayout.Space();
        }

        void BuildingGUI()
        {
            int prevInputCRSIndex = config_UI.inputCRSIndex;
            int prevOutputCRSIndex = config_UI.outputCRSIndex;

            config_UI.buildingProvider = (BuldingProvider)EditorGUILayout.EnumPopup("Building Provider", config_UI.buildingProvider);

            
            if (config_UI.buildingProvider == BuldingProvider.CityGML)
            {
                if (GUILayout.Button("Select GML file"))
                {
                    string folderPath = ARRCPreference.LoadPreference(ARRCPrefPathID.CityGMLFolder);
                    string cityGMLFilepath = EditorUtility.OpenFilePanelWithFilters("Select citygml file", folderPath, new string[] { "*", "gml" });
                    config_UI.filepath_citygml = cityGMLFilepath;
                    
                    string CRSCode = "";
                    //좌표계 로드
                    (double x, double y, double z) lowerRange = (0, 0, 0), upperRange = (0, 0, 0);
                    CityGMLParsor.GetCoordinateInfo(config_UI.filepath_citygml, ref CRSCode, ref lowerRange, ref upperRange);

                    config_UI.cityGMLCRSIndex = CoordinateDB.GetCRSIndex(CRSCode);
                    config_UI.inputCRSIndex = config_UI.cityGMLCRSIndex;
                    config_UI.inputRange = (lowerRange.x, lowerRange.z, upperRange.x, upperRange.z);
                    
                }
                if(config_UI.cityGMLCRSIndex != -1)
                {
                    if (config_UI.useBuildingCRS)
                        config_UI.outputCRSIndex = config_UI.cityGMLCRSIndex;

                    string CRSName = CoordinateDB.GetCRSCode(config_UI.cityGMLCRSIndex);

                    EditorGUILayout.LabelField("CityGML path : " + config_UI.filepath_citygml, EditorStyles.boldLabel);
                    config_UI.useBuildingCRS = EditorGUILayout.Toggle("Output CRS : " + CRSName, config_UI.useBuildingCRS);
                    config_UI.useSharedMemory = EditorGUILayout.Toggle("Use shared memory", config_UI.useSharedMemory);
                }
                else
                {
                    EditorGUILayout.LabelField("First you need to select GML file.");
                }
            }

            else if(config_UI.buildingProvider == BuldingProvider.VWorld)
            {
                config_UI.useBuildingCRS = EditorGUILayout.Toggle("Output CRS : UTMK", config_UI.useBuildingCRS);

                if(config_UI.useBuildingCRS)
                {
                    config_UI.outputCRSIndex = CoordinateDB.GetCRSIndex(CoordinateDB.GetCRSCode("UTMK"));
                    config_UI.useSharedMemory = EditorGUILayout.Toggle("Use shared memory", config_UI.useSharedMemory);
                }
                    
            }

            if (prevInputCRSIndex != config_UI.inputCRSIndex)
                ResetConverterByInputChange(prevInputCRSIndex);

            if (prevOutputCRSIndex != config_UI.outputCRSIndex)
                ResetConverterByOutputChanged();

        }

        void StreetViewGUI()
        {
            config_UI.downloadStreetViewImages = EditorGUILayout.Toggle("Download Color Images", config_UI.downloadStreetViewImages);
            if (config_UI.downloadStreetViewImages) config_UI.targetZoom = EditorGUILayout.IntSlider("Zoom", config_UI.targetZoom, 1, 5);

            if (config_UI.streetviewProvider == StreetViewProvider.Google)
                config_UI.downloadStreetViewDepthImages = EditorGUILayout.Toggle("Download Depth Images", config_UI.downloadStreetViewDepthImages);

            config_UI.streetviewProvider = (StreetViewProvider)EditorGUILayout.EnumPopup("Provider", config_UI.streetviewProvider);
        }

        void PointCloudGUI()
        {
            if (GUILayout.Button("Select .ply file"))
            {
                //EditorUtility.DisplayDialog("Select Texture", "You must select a texture first!", "OK");
                string path = EditorUtility.OpenFilePanel("Select .ply file", Application.streamingAssetsPath, "ply");
                //int LastIndex = path.LastIndexOf ("/");
                //mTotalPath = path;
                config_UI.filepath_pointcloud = path;

            }
            if (config_UI.filepath_pointcloud != "")
            {
                EditorGUILayout.LabelField("filepath path : " + config_UI.filepath_pointcloud, EditorStyles.boldLabel);


                if (GUILayout.Button("Generate PointCloud"))
                {
                    GameObject parent = GameObject.Find("Point Cloud");
                    if (parent == null)
                        parent = new GameObject("Point Cloud");

                    PointCloudGeneratorWarpper.Instance.BuildCloud(config_UI.filepath_pointcloud, parent.transform);
                }
            }
        }

    
        public bool CheckCoordValidation((double minX, double minY, double maxX, double maxY) range)
        {
            if (range.minX >= range.maxX || range.minY >= range.maxY)
                return false;

            return true;
        }

        static ARRCDigitalTwinGeneratorWindow instance;
        public static ARRCDigitalTwinGeneratorWindow Instance
        {
            get
            {
                if (instance)
                    return instance;
                else
                {
                    instance = GetWindow<ARRCDigitalTwinGeneratorWindow>(false, "Digital Twin Generator");
                    return instance;
                }
            }
        }

        
        [MenuItem("ARRC/Digital Twin Generator")]
        public static ARRCDigitalTwinGeneratorWindow OpenWindow()
        {
            return Instance;
        }

    }
}

