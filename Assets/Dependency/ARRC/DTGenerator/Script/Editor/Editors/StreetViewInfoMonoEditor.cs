﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    [CustomEditor(typeof(StreetViewItemMono)), CanEditMultipleObjects]
    public class StreetViewInfoMonoEditor : Editor
    {
        [Range(1, 5)]
        public int zoom_to_download = 1;

        StreetViewTaskManager taskManager = StreetViewTaskManager.Instance;

        StreetViewItemMono[] selectedObjects;

        bool[] fold;

        private void OnEnable()
        {
            EditorApplication.update += Update;

            fold = new bool[targets.Length];
            if (fold.Length == 1) fold[0] = true;

            selectedObjects = new StreetViewItemMono[targets.Length];
            for (int i = 0; i < targets.Length; i++)
                selectedObjects[i] = targets[i] as StreetViewItemMono;
        }

        void OnDisable() { EditorApplication.update -= Update; }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();


            if (taskManager.IsEmpty)
            {
                EditorGUILayout.LabelField("Zoom to download : ");
                zoom_to_download = EditorGUILayout.IntSlider(zoom_to_download, 1, 5);

                var goList = from mono in selectedObjects
                             select mono.gameObject;


                if (GUILayout.Button("Load color image"))
                {
                    taskManager.Initialize(selectedObjects[0], goList.ToList(), TextureType.Color, zoom_to_download);
                }
                if (GUILayout.Button("Load depth image"))
                {
                    taskManager.Initialize(selectedObjects[0], goList.ToList(), TextureType.Depth);
                }

                if (GUILayout.Button("Load label image"))
                {
                    taskManager.Initialize(selectedObjects[0], goList.ToList(), TextureType.Label);
                }
            }
            else
            {
                //DrawStreetViewInfo(workingObjects);
                if (GUILayout.Button("Cancel"))
                {
                    taskManager.Dispose();
                }

                else
                {
                    int phaseCount = taskManager.TaskCount;
                    float progress = 0;

                    for (int i = 0; i < phaseCount; i++)
                    {
                        if (i < taskManager.ActiveIndex)
                            progress = 1;

                        else if (i == taskManager.ActiveIndex)
                            progress = taskManager.ProgressOfActiveTask;

                        else if (i > taskManager.ActiveIndex)
                            progress = 0;

                        float totalSize = taskManager.TotalSizeOf(i);
                        int completedSize = Mathf.FloorToInt(totalSize * progress);
                        string phaseState = taskManager.StateOf(i);
                        TaskType taskType = taskManager.TaskTypeOf(i);

                        EditorGUILayout.LabelField(phaseState);
                        string strProgress = Mathf.FloorToInt(taskManager.ProgressOfActiveTask * 100).ToString();

                        if (taskType == TaskType.Download)
                            strProgress += " (" + completedSize + " of " + totalSize + " mb)";

                        Rect r = EditorGUILayout.BeginVertical();
                        EditorGUI.ProgressBar(r, progress, strProgress + "%");
                        EditorGUILayout.EndVertical();
                    }
                    EditorUtility.SetDirty(target); //다시 그림
                }
            }

            DrawStreetViewInfos(selectedObjects);
        }


        public void Update()
        {
            taskManager.Update();

            if (taskManager.IsCompleted)
                taskManager.Dispose();
        }

        public void DrawStreetViewInfos(StreetViewItemMono[] items)
        {

            EditorGUILayout.Space();
            GUILayout.Label("Count : " + items.Length.ToString());

            for (int i = 0; i < items.Length && items.Length == 1; i++)
            {
                try
                {
                    fold[i] = EditorGUILayout.Foldout(fold[i], items[i].streetviewInfo.panoid, true);
                    if (fold[i])
                    {
                        EditorGUI.indentLevel++;
                        GUILayout.Label("Latitude  : " + items[i].streetviewInfo.lat);
                        GUILayout.Label("Longitude : " + items[i].streetviewInfo.lon);
                        GUILayout.Label("Elevation_wgs84_m : " + items[i].streetviewInfo.elevation_wgs84_m);
                        GUILayout.Label("elevation_egm96_m : " + items[i].streetviewInfo.elevation_egm96_m);
                        GUILayout.Label("Image date : " + items[i].streetviewInfo.image_date);
                        GUILayout.Label("Contury : " + items[i].streetviewInfo.country);
                        GUILayout.Label("Region : " + items[i].streetviewInfo.region);
                        GUILayout.Label("Text : " + items[i].streetviewInfo.text);
                        GUILayout.Label("Loaded zoomlevel : " + items[i].streetviewInfo.loadedZoom);
                        GUILayout.Label("Texture path: " + items[i].loadedTexturePath);
                        EditorGUI.indentLevel--;
                    }

                }
                catch
                {
                    break;
                }
            }
        }

        public void DrawLocationInfo(ARRCGeneratorMonoBase item)
        {
            //GUILayout.Label("Reference Coordinate : " + item.coordinateInfo.crsName_target);

            //GUILayout.Label("Min X : " + item.coordinateInfo.targetBoundary.minX);
            //GUILayout.Label("Min Y : " + item.coordinateInfo.targetBoundary.minY);
            //GUILayout.Label("Max X : " + item.coordinateInfo.targetBoundary.maxX);
            //GUILayout.Label("Max Y : " + item.coordinateInfo.targetBoundary.maxY);
            //EditorGUILayout.Space();
        }
    }
}


