﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using ARRC.Commons;


namespace ARRC.DigitalTwinGenerator
{
    public class DTGenTaskManager : TaskManager
    {
        DownloadManager downloadManager = new DownloadManager();

        public void Initialize(DTConfigurationItem config, EditorWindow wnd)
        {
            base.Initialize();

            string WGS84Code = CoordinateDB.GetCRSCode("WGS84");
            string inputCRSCode = CoordinateDB.GetCRSCode(config.inputCRSIndex);
            string outputCRSCode = CoordinateDB.GetCRSCode(config.outputCRSIndex);

            (double minLon, double minLat, double maxLon, double maxLat) inputRange_wgs84 = config.inputRange;
            // source-> wgs84 으로 좌표계 변환하여 입력 영역 구하기.
            if (inputCRSCode != WGS84Code)
            {
                CoordinateInfo coordConvertor_sourceToWgs84 = new CoordinateInfo();
                coordConvertor_sourceToWgs84.InitConverter(inputCRSCode, WGS84Code);
                coordConvertor_sourceToWgs84.SetTargetBoundary(config.inputRange);
                inputRange_wgs84 = coordConvertor_sourceToWgs84.targetBoundary;
            }

            // wgs84 -> target 으로 좌표계 변환.
            CoordinateInfo coordConvertor_wgs84ToTarget = new CoordinateInfo();
            coordConvertor_wgs84ToTarget.InitConverter(WGS84Code, outputCRSCode);
            coordConvertor_wgs84ToTarget.SetTargetBoundary(inputRange_wgs84);


            if (config.generateBuildings)
            {
                if (config.buildingProvider == BuldingProvider.VWorld)
                {
                    var vworldBuildingGen = new VWorldBuildingGenerator();

                    AddTask(new CoroutineEditorTask("Making a download list of 'list of xdo files' ...", wnd, vworldBuildingGen, vworldBuildingGen.MakeDownloadItemList_XDODataList(config.title, inputRange_wgs84, downloadManager)));
                    AddTask(new DownloadTask("Downloading the 'list of xdo files'...", downloadManager));
                    AddTask(new CoroutineEditorTask("Parsing the list and making a download list of 'xdo file'...", wnd, vworldBuildingGen, vworldBuildingGen.MakeDownloadItemList_XDOData(downloadManager)));
                    AddTask(new DownloadTask("Downloading the xdo files ...", downloadManager));
                    AddTask(new CoroutineEditorTask("Writing after converting 'xdo files' to 'obj files' ...", wnd, vworldBuildingGen, vworldBuildingGen.WriteOBJFiles(downloadManager)));
                    AddTask(new DownloadTask("Downloading textures for each obj files...", downloadManager));

                    if (!config.useSharedMemory)
                        AddTask(new CoroutineEditorTask("Import obj files to result folder...", wnd, vworldBuildingGen, vworldBuildingGen.ImportModelstoAssets(config.title)));

                    AddTask(new CoroutineEditorTask("Generating gameobjects for each obj files ...", wnd, vworldBuildingGen, vworldBuildingGen.CreateBuilding(config.title, coordConvertor_wgs84ToTarget, config.useSharedMemory)));
                }
                else if (config.buildingProvider == BuldingProvider.CityGML)
                {
                    string CRSCode = null;
                    (double x, double y, double z) lowerRange = (0, 0, 0), upperRange = (0, 0, 0);
                    CityGMLParsor.GetCoordinateInfo(config.filepath_citygml, ref CRSCode, ref lowerRange, ref upperRange);

                    // source-> gml 으로 좌표계 변환하여 입력 영역 구하기.
                    (double minX, double minY, double maxX, double maxY) inputRange_gml = config.inputRange;

                    if (inputCRSCode != CRSCode)
                    {
                        CoordinateInfo coordConvertor_sourceTogml = new CoordinateInfo();
                        coordConvertor_sourceTogml.InitConverter(inputCRSCode, WGS84Code);
                        coordConvertor_sourceTogml.SetTargetBoundary(config.inputRange);
                        inputRange_gml = coordConvertor_sourceTogml.targetBoundary;
                    }

                    // gml -> target 으로 좌표계 변환.
                    CoordinateInfo coordConvertor_gmlToTarget = new CoordinateInfo();
                    coordConvertor_gmlToTarget.InitConverter(CRSCode, outputCRSCode);
                    coordConvertor_gmlToTarget.SetTargetBoundary(inputRange_gml);

                    var cityGMLGen = new CityGMLGenerator();

                    //ParseCityGMLProperties
                    AddTask(new CoroutineEditorTask("Creating gameobjects from GML files ...", wnd, cityGMLGen, cityGMLGen.BuildGML(config.title, config.filepath_citygml, inputRange_gml, coordConvertor_gmlToTarget, config.useSharedMemory)));

                    //Generate CityGML Objects.
                }
            }

            //StreetView 관련
            if (config.generateStreetViews)
            {
                var svGen = new StreetViewGenerator();
                var streetViewInfoList = new List<StreetViewItem>();
                var imgObjList = new List<GameObject>();

                AddTask(new CoroutineEditorTask("Making a download list of metadata ...", wnd, svGen, svGen.AddDownloadItemsForMetaData_fromRange(config.title, inputRange_wgs84, downloadManager)));
                AddTask(new DownloadTask("Downloading list of the metadata ...", downloadManager, 16));
                AddTask(new CoroutineEditorTask("Creating streetviewinfo for metadata ...", wnd, svGen, svGen.AddStreetViewInfoFromFolder(streetViewInfoList, config.title)));
                AddTask(new CoroutineEditorTask("Creating gameobjects for streetviewinfos ...", wnd, svGen, svGen.CreateStreetViewObjects(streetViewInfoList, imgObjList, coordConvertor_wgs84ToTarget)));

                if (config.downloadStreetViewImages)
                {
                    AddTask(new CoroutineEditorTask("Making a download list of image fragments ...", wnd, svGen, svGen.AddDownloadItemsForPartImages(streetViewInfoList, downloadManager, config.targetZoom)));
                    AddTask(new DownloadTask("Downloading the image fragments ...", downloadManager, 4));
                    AddTask(new CoroutineEditorTask("Importing images after merging fragments into one ...", wnd, svGen, svGen.WriteStreetViewColors(streetViewInfoList, config.targetZoom)));
                    AddTask(new CoroutineEditorTask("Loading the images into gameobjects...", wnd, svGen, svGen.LoadTextures(imgObjList, TextureType.Color, config.targetZoom)));
                }
                if (config.downloadStreetViewDepthImages)
                {
                    AddTask(new CoroutineEditorTask("Converting and Importing depth data ...", wnd, svGen, svGen.WriteStreetViewDepths(streetViewInfoList, 255, true, true, true)));
                }
            }


            if (config.generateKCTMMetaData)
            {
                //not yet.
            }
        }

        public override void Dispose()
        {
            base.Dispose();
            downloadManager = null;
        }

        static DTGenTaskManager instance;
        public static DTGenTaskManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new DTGenTaskManager();

                return instance;
            }
        }
    }
}