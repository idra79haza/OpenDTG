﻿using ARRC.Commons;
using System;
using UnityEngine;

namespace ARRC.DigitalTwinGenerator
{
    [Serializable]
    public class StreetViewItem : ARRCResultItem
    {
        [SerializeField]
        DictionaryOfStringAndString properties;

        public int loadedZoom = -1;

        public StreetViewItem(DictionaryOfStringAndString properties, string folderName) : base(folderName)
        {
            this.properties = properties;
        }

        public string panoid
        {
            get
            {
                if (properties != null && properties.ContainsKey("pano_id"))
                    return properties["pano_id"];
                else
                    return "";
            }
        }

        public string lat { get { return properties["lat"];}}
        public string lon { get { return properties["lng"];}}

        public string original_lat { get { return properties["original_lat"]; } }
        public string original_lng { get { return properties["original_lng"]; } }

        public string elevation_wgs84_m { get { return properties["elevation_wgs84_m"];}}
        public string elevation_egm96_m { get { return properties["elevation_egm96_m"]; } }
        public string tilt_pitch_deg { get { return properties["tilt_pitch_deg"]; }}
        public string pano_yaw_deg { get { return properties["pano_yaw_deg"]; }}
        public string image_date { get { return properties["image_date"]; } }

        public int image_width { get { return int.Parse(properties["image_width"]); } }
        public int image_height { get { return int.Parse(properties["image_height"]); } }

        public int tile_width { get { return int.Parse(properties["tile_width"]); } }
        public int tile_height { get { return int.Parse(properties["tile_height"]); } }


        public int num_zoom_levels { get { return int.Parse(properties["num_zoom_levels"]); } }
        public double best_view_direction_deg { get { return double.Parse(properties["best_view_direction_deg"]); } }

        //public int loadedZoom 
        //{
            
        //    get {
        //        if (properties != null && properties.ContainsKey("zoom"))
        //            return int.Parse(properties["zoom"]);
        //        else
        //            return -1;
        //    }  
        //    set { properties["zoom"] = value.ToString(); } 
        //}

        public string depth_map { get { return properties["depth_map"]; } }

        public string text { get { return properties["text"]; } }
        public string region { get { return properties["region"]; } }
        public string country { get { return properties["country"]; } }



   
        public static string[] properties_data = {"image_width", "image_height", "tile_width", "tile_height",
                "image_date", "pano_id", "imagery_type", "num_zoom_levels",
                "lat", "lng", "original_lat", "original_lng",
                "elevation_wgs84_m","best_view_direction_deg", "elevation_egm96_m"};

        public static string[] properties_projection = { "pano_yaw_deg", "tilt_yaw_deg", "tilt_pitch_deg" };
        public static string[] properties_etc = { "text", "region", "country", "depth_map" }; //  안씀. Just 참고용.

        public void CalculateTiles(int targetZoom, out int tile_num_X, out int tile_num_Y, out int tile_width_practical, out int tile_height_practical, out int tile_num_X_practical, out int tile_num_Y_practical)
        {

            tile_num_X = (int)Math.Pow(2, targetZoom); 
            tile_num_Y = (int)Math.Pow(2, targetZoom - 1); 

            int max_tile_num_X = (int)Math.Pow(2, num_zoom_levels);
            int max_tile_num_Y = (int)Math.Pow(2, num_zoom_levels - 1);

            tile_width_practical = image_width / max_tile_num_X;
            tile_height_practical = image_height / max_tile_num_Y;

            tile_num_X_practical = (int) Math.Ceiling(tile_num_X * tile_width_practical / (double) tile_width);
            tile_num_Y_practical = (int) Math.Ceiling(tile_num_Y * tile_height_practical / (double)tile_height);

        }
    }

}
;