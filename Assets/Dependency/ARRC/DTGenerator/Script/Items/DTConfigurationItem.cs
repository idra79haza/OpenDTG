﻿
using System.Xml;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    public class DTConfigurationItem : ARRCXMLItem
    {
        ////Berlin1
        //public string title;
    
        public (double minX, double minY, double maxX, double maxY) inputRange
        {
            get
            {
                return (input_minX, input_minY, input_maxX, input_maxY);
            }
            set
            {
                input_minX = value.minX;
                input_minY = value.minY;
                input_maxX = value.maxX;
                input_maxY = value.maxY;
            }
        }
        public (double minX, double minY, double maxX, double maxY) outputRange
        {
            get
            {
                return (output_minX, output_minY, output_maxX, output_maxY);
            }
            set
            {
                output_minX = value.minX;
                output_minY = value.minY;
                output_maxX = value.maxX;
                output_maxY = value.maxY;
            }
        }

        //public (double minX, double minY, double maxX, double maxY) inputRange
        //    = (13.377621, 52.511065, 13.391103, 52.519203);

        //public string title = "berlin";
        //double input_minX = 13.377621;
        //double input_minY = 52.511065;
        //double input_maxX = 13.391103;
        //double input_maxY = 52.519203;

        //VWorld로 다운 받아둔 좌표.(지우지 말것)
        //관덕정
        //public string title = "Gwanduk";
        //double input_minX = 126.52084843292232;
        //double input_minY = 33.51296418982448;
        //double input_maxX = 126.52331968688964;
        //double input_maxY = 33.515014847067846;

        //경복궁
        //public string title = "Gyungbok";
        //public (double minX, double minY, double maxX, double maxY) inputRange 
        //    = (126.974575, 37.5758232, 126.9775363, 37.5776476);


        //둔산동 갤러리아(entire)
        //public string title = "doonsan";
        //public (double minX, double minY, double maxX, double maxY) inputRange
        //    = (127.3717533, 36.3512583, 127.3774753, 36.3561183);
        //double input_minX = 127.3717533, input_minY = 36.3512583, input_maxX = 127.3774753, input_maxY = 36.3561183;


        //둔산동 갤러리아 (part1)
        //public string title = "Doonsan_part1";
        //double minLat = 36.3512583;
        //double minLon = 127.374431;
        //double maxLat = 36.352615;
        //double maxLon = 127.3774753;

        public string title;
        double input_minX, input_minY, input_maxX, input_maxY;
        double output_minX, output_minY, output_maxX, output_maxY;

        public int inputCRSIndex = 0; //WGS84
        public int outputCRSIndex = 0; 
        public int cityGMLCRSIndex = -1;

        public bool useBuildingCRS = true;

        public TerrainElevationProvider elevationProvider = TerrainElevationProvider.VWorld;
        public TerrainTextureProvider textureProvider = TerrainTextureProvider.VWorld;
        public BuldingProvider buildingProvider = BuldingProvider.CityGML;
        public StreetViewProvider streetviewProvider = StreetViewProvider.Google;

        public bool generateStreetViews = false;
        public bool generateTerrains = false;
        public bool generateBuildings = false;
        public bool generatePointCloud = false;
        public bool generateKCTMMetaData = false;
        public bool downloadStreetViewImages = false;
        public bool downloadStreetViewDepthImages = false;

        public int targetZoom = 1;

        public string filepath_citygml;
        public string filepath_pointcloud;
        public bool useSharedMemory = true;

        public DTConfigurationItem()
        {
            outputRange = inputRange;
        }

        public DTConfigurationItem(XmlNode node) : base(node)
        {

        }
        public DTConfigurationItem DeepCopy()
        {
            DTConfigurationItem other = (DTConfigurationItem) MemberwiseClone();
            return other;

        }
        public override void AppendToXML(string xmlpath)
        {
            base.AppendToXML(xmlpath);
        }
       
    }
}
