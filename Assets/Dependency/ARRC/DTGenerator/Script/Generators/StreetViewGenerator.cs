﻿using PrimitivesPro.GameObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using UnityEngine;
using zlib;
using ARRC.Commons;
using ARRC.ARRCTexture;

#if UNITY_EDITOR
using UnityEditor;
#endif   

namespace ARRC.DigitalTwinGenerator
{
    public class StreetViewGenerator : ARRCGenerator
    {
        const double STEP_SIZE = 0.00005; //지도에서 StreetView 검색 간격.
        const string url_metadata = "http://maps.google.com/cbk?output=xml&ll={0},{1}&dm=1";
        const string url_metadata_panoid = "http://cbk0.google.com/cbk?output=xml&panoid={0}&dm=1";
        const string url_partImage = "http://maps.google.com/cbk?output=tile&panoid={0}&zoom={1}&x={2}&y={3}";


        public IEnumerator AddDownloadItemsForMetaData_fromRange(string title, (double minLon, double minLat, double maxLon, double maxLat) wgs84Range, DownloadManager downloadManager)
        {
            //CurrentState = "Composing download list for metadata...";

            string subFolder_xml = Path.Combine(ARRCPaths.CachFolder_StreetView_XML, title);
            if (!Directory.Exists(subFolder_xml))
            {
                //StreetView List
                double total_progress = (wgs84Range.maxLat - wgs84Range.minLat);
                int count_progress = 0;
                totalCount = (int)(total_progress / STEP_SIZE);

                for (double lat = wgs84Range.minLat; lat <= wgs84Range.maxLat; lat += STEP_SIZE)
                {
                    for (double lon = wgs84Range.minLon; lon <= wgs84Range.maxLon; lon += STEP_SIZE)
                    {
                        string url = string.Format(url_metadata, lat, lon);

                        downloadManager.Add(new DownloadItem_StreetViewInfo(url)
                        {
                            //filename = filepath_xmlList,
                            directory = subFolder_xml,
                            averageSize = ARRCDataSize.AVERAGE_STREETVIEW_XML_SIZE
                        });
                    }

                    progress = (float)((++count_progress * STEP_SIZE) / total_progress);
                    yield return null;
                }
                //downloadItems = downloadItemList_streetviewXML;
            }

            isComplete = true;
        }
        public IEnumerator AddDownloadItemsForMetaData_fromPanoIDList(string title, string[] panoids, DownloadManager downloadManager)
        {
            currentState = "dd";
            totalCount = panoids.Length;
            float count = 0;

            foreach (string panoid in panoids)
            {
                string url = string.Format(url_metadata_panoid, panoid);
                string subFolder_xml = Path.Combine(ARRCPaths.CachFolder_StreetView_XML, title);

                downloadManager.Add(new DownloadItem_StreetViewInfo(url)
                {
                    directory = subFolder_xml,
                    averageSize = ARRCDataSize.AVERAGE_STREETVIEW_XML_SIZE
                });
                progress = ++count / totalCount;

            }
            yield return null;
            isComplete = true;
        }

        public IEnumerator AddStreetViewInfoFromFolder(List<StreetViewItem> targetStreetViewInfoList, string forderName)
        {

            string filepath_xmlList = Path.Combine(ARRCPaths.CachFolder_StreetView_XML, forderName);
            string[] fileapaths = Directory.GetFiles(filepath_xmlList, "*.xml");


            totalCount = fileapaths.Length;

            for (int i = 0; i < fileapaths.Length; i++)
            {
                targetStreetViewInfoList.Add(CreateStreetViewInfoFromFile(fileapaths[i], forderName));

                progress = (float)(i + 1) / fileapaths.Length;

                yield return null;
            }
            isComplete = true;
        }

        public IEnumerator AddStreetViewInfoFromFilename(List<StreetViewItem> targetStreetViewInfoList, string searchFolder, string[] filenames)
        {
            totalCount = filenames.Length;

            float count = 0;

            foreach (string filename in filenames)
            {
                string xmlFolder = Path.Combine(ARRCPaths.CachFolder_StreetView_XML, searchFolder);
                string[] fileapaths = Directory.GetFiles(xmlFolder, filename + ".xml");

                targetStreetViewInfoList.Add(CreateStreetViewInfoFromFile(fileapaths[0], searchFolder));

                progress = (++count / fileapaths.Length);

                yield return null;
            }
            isComplete = true;

        }
        public IEnumerator CreateStreetViewObjects(List<StreetViewItem> streetViewInfoList, List<GameObject> targetImgObjList, CoordinateInfo wgs84ToTarget)
        {
            //CurrentState = "Creating gameobjects for streetview images...";

            totalCount = streetViewInfoList.Count;

            GameObject parent_Images = new GameObject("SteetViews");
            parent_Images.AddComponent<ResizeChildObjectMono>();

            //현재 멤버변수 streetViewInfoList를 참조하여 GameObject들을 생성한다.
            for (int i = 0; i < streetViewInfoList.Count; i++)
            {
                GameObject objImg = CreateStreetViewObject(streetViewInfoList[i], wgs84ToTarget);
                GameObject objCam = CreateCameraObject(objImg.transform);

                objImg.transform.parent = parent_Images.transform;
                objCam.transform.parent = objImg.transform;

                objImg.GetComponent<Renderer>().enabled = false;

                //Attach StreetViewItemMono.
                objImg.AddComponent<StreetViewItemMono>();
                //objImg.GetComponent<StreetViewItemMono>().title = config.title;
                objImg.GetComponent<StreetViewItemMono>().coordinateInfo = wgs84ToTarget;
                objImg.GetComponent<StreetViewItemMono>().streetviewInfo = streetViewInfoList[i];

                //Attach StreetViewControllerMono.
                objImg.AddComponent<StreetViewControllerMono>();

                targetImgObjList.Add(objImg);

                progress = (float)(i + 1) / streetViewInfoList.Count;

                yield return null;
            }
            isComplete = true;

        }
        public IEnumerator AddDownloadItemsForPartImages(List<StreetViewItem> streetViewInfoList, DownloadManager downloadManager, int targetZoom)
        {
            //CurrentState = "Composing download list for parts of mages...";
            totalCount = streetViewInfoList.Count;

            for (int i = 0; i < streetViewInfoList.Count; i++)
            {
                AddPartImageDownloadList(streetViewInfoList[i], targetZoom, downloadManager);

                progress = (float)(i + 1) / streetViewInfoList.Count;
                yield return null;
            }

            isComplete = true;
        }

        public IEnumerator WriteStreetViewColors(List<StreetViewItem> streetViewInfoList, int targetZoom)
        {
            //CurrentState = "Merging and then Writing part of the images ...";

            streetViewInfoList[0].CalculateTiles(targetZoom, out int tile_num_X_dummy, out int tile_num_Y_dummy, out int tile_width_practical_dummy, out int tile_height_practical_dummy, out int tile_num_X_practical_dummy, out int tile_num_Y_practical_dummy);

            totalCount = streetViewInfoList.Count * tile_num_Y_practical_dummy * tile_num_X_practical_dummy; // 첫번재 streetview 에 대한 tile_y 개수 이므로 추정치임.

            for (int z = 0; z < streetViewInfoList.Count; z++)
            {
                StreetViewItem svInfo = streetViewInfoList[z];
                string fullfilepath = ARRCPaths.ResultFolder_StreetView_Color(svInfo.folderName, targetZoom) + "/" + svInfo.panoid + ".png";
                FileInfo info = new FileInfo(fullfilepath);

                if (!info.Exists)
                {
                    if (!info.Directory.Exists)
                        info.Directory.Create();

                    svInfo.CalculateTiles(targetZoom, out int tile_num_X, out int tile_num_Y, out int tile_width_practical, out int tile_height_practical, out int tile_num_X_practical, out int tile_num_Y_practical);

                    //tileSize
                    int tile_width = svInfo.tile_width;
                    int tile_height = svInfo.tile_height;

                    //실제 크기.
                    int width_texture_practical = tile_width_practical * tile_num_X;
                    int height_texture_pracical = tile_height_practical * tile_num_Y;

                    //이론적인 크기.
                    int width_texture = tile_width * tile_num_X;
                    int height_texture = tile_height * tile_num_Y;

                    Texture2D texture_pratical = new Texture2D(width_texture_practical, height_texture_pracical, TextureFormat.RGB24, false);
                    Texture2D tempTexture = new Texture2D(width_texture, height_texture, TextureFormat.RGB24, false);

                    for (int y = 0; y < tile_num_Y_practical; y++)
                    {
                        for (int x = 0; x < tile_num_X_practical; x++)
                        {
                            Texture2D tempPartTexture = new Texture2D(tile_width, tile_height);
                            tempPartTexture.wrapMode = TextureWrapMode.Clamp;
                            string filepath = GetStreetViewCacheFilePath(targetZoom, svInfo.panoid, x, y);
                            FileInfo info_fragment = new FileInfo(filepath);

                            if (info_fragment.Exists)
                            {
                                tempPartTexture.LoadImage(File.ReadAllBytes(filepath));
                                tempTexture.SetPixels(x * tile_width, (tile_num_Y - y - 1) * tile_height, tile_width, tile_height, tempPartTexture.GetPixels());
                            }
                            else
                            {
                                Debug.LogWarningFormat("The file \"{0}\" is not found, creation of \"{1}\" is skipped.", filepath, info.Name);
                                goto SkipThisImage;
                            }

                        }
                        progress = (float)(z * tile_num_Y_practical * tile_num_X_practical + y * tile_num_X_practical) / totalCount;
                        yield return null; //가로줄이 끝날때마다 yield.
                    }

                    texture_pratical.SetPixels(tempTexture.GetPixels(0, (tile_height - tile_height_practical) * tile_num_Y, width_texture_practical, height_texture_pracical));
                    texture_pratical.Apply();

                    File.WriteAllBytes(info.FullName, texture_pratical.EncodeToPNG());
                    UnityEditor.AssetDatabase.Refresh();

                SkipThisImage:
                    continue;
                }

            }
            isComplete = true;
        }

        public IEnumerator WriteStreetViewDepths(List<StreetViewItem> streetViewInfoList, float maxDist, bool writePseudoDepth, bool writeTurboColor, bool writeGrayScale)
        {
            //CurrentState = "Decoding and Writing the depth data ...";

            int count = 0;
            totalCount = streetViewInfoList.Count;

            foreach (StreetViewItem svInfo in streetViewInfoList)
            {
                string fullfilepath_pseudoDepth = ARRCPaths.GetPseudoDepthPath(svInfo.folderName, svInfo.panoid + ".exr");
                string fullfilepath_turboColor = ARRCPaths.GetTurboColorPath(svInfo.folderName, svInfo.panoid + ".png");
                string fullfilepath_grayScale = ARRCPaths.GetGrayScaleDepthPath(svInfo.folderName, svInfo.panoid + ".exr");


                FileInfo info_pseudoDepth = new FileInfo(fullfilepath_pseudoDepth);
                FileInfo info_turboColor = new FileInfo(fullfilepath_turboColor);
                FileInfo info_grayScale = new FileInfo(fullfilepath_grayScale);


                //if ((!info_pseudoDepth.Exists && writePseudoDepth) ||
                //     (!info_turboColor.Exists && writeTurboColor) ||
                //     (!info_grayScale.Exists && writeGrayScale))
                {
                    if (!info_pseudoDepth.Directory.Exists && writePseudoDepth)
                        info_pseudoDepth.Directory.Create();

                    if (!info_turboColor.Directory.Exists && writeTurboColor)
                        info_turboColor.Directory.Create();

                    if (!info_grayScale.Directory.Exists && writeGrayScale)
                        info_grayScale.Directory.Create();

                    string fixed_base64 = svInfo.depth_map;

                    //# fix the 'inccorrect padding' error. The length of the string needs to be divisible by 4.
                    int addedStringCount = ((4 - fixed_base64.Length % 4) % 4);

                    for (int i = 0; i < addedStringCount; i++)
                        fixed_base64 += "=";

                    fixed_base64 = fixed_base64.Replace('-', '+').Replace('_', '/');

                    byte[] bytes_compressed = Convert.FromBase64String(fixed_base64);
                    byte[] bytes_uncompressed = new byte[bytes_compressed.Length];
                    DecompressData(bytes_compressed, out bytes_uncompressed);

                    //Make depth image file.
                    int headersize = bytes_uncompressed[0];
                    int numberofplanes = bytes_uncompressed[1] | (bytes_uncompressed[2] << 8);
                    int width = bytes_uncompressed[3] | (bytes_uncompressed[4] << 8);
                    int height = bytes_uncompressed[5] | (bytes_uncompressed[6] << 8);
                    int offset = bytes_uncompressed[7];

                    if (headersize != 8 || offset != 8)
                    {
                        throw new Exception("Unexpected depth map header ");
                    }

                    //the values in this vector correspondent to a planeId this pixel belongs to
                    //depthMapIndices
                    byte[] depthmapIndices = new byte[height * width];
                    Buffer.BlockCopy(bytes_uncompressed, offset, depthmapIndices, 0, height * width);

                    //depthMapPlanes
                    byte[] depthmapPlanesBuffer = new byte[numberofplanes * 4 * sizeof(float)];
                    Buffer.BlockCopy(bytes_uncompressed, offset + height * width, depthmapPlanesBuffer, 0, numberofplanes * 4 * sizeof(float));

                    Vector4[] depthmapPlanes = new Vector4[numberofplanes];

                    for (int pi = 0; pi < numberofplanes; pi++)
                    {
                        //normal vector
                        depthmapPlanes[pi].x = BitConverter.ToSingle(depthmapPlanesBuffer, pi * 4 * sizeof(float) + 0);
                        depthmapPlanes[pi].y = BitConverter.ToSingle(depthmapPlanesBuffer, pi * 4 * sizeof(float) + 1 * sizeof(float));
                        depthmapPlanes[pi].z = BitConverter.ToSingle(depthmapPlanesBuffer, pi * 4 * sizeof(float) + 2 * sizeof(float));
                        //distance to camera
                        depthmapPlanes[pi].w = BitConverter.ToSingle(depthmapPlanesBuffer, pi * 4 * sizeof(float) + 3 * sizeof(float));
                    }

                    //memcpy(&depthmapPlanes[0], &depth_map[offset + height * width], numberofplanes * sizeof(struct DepthMapPlane));

                    //CREATE THE ACTUAL DEPTHMAP

                    Color[] colorArray_pseudoDepth = new Color[width * height];
                    Color[] colorArray_grayScale = new Color[width * height];
                    Color[] colorArray_turboColor = new Color[width * height];
                    for (int y = 0; y < height; ++y)
                    {
                        for (int x = 0; x < width; ++x)
                        {
                            float rad_azimuth = x / (width - 1.0f) * Mathf.PI * 2;
                            float rad_elevation = y / (height - 1.0f) * Mathf.PI;

                            //convert from spherical to cartesian coordinates
                            Vector3 pos = new Vector3
                            {
                                x = Mathf.Sin(rad_elevation) * Mathf.Sin(rad_azimuth),
                                y = Mathf.Sin(rad_elevation) * Mathf.Cos(rad_azimuth),
                                z = Mathf.Cos(rad_elevation)
                            };

                            int planeIdx = depthmapIndices[y * width + x];

                            if (planeIdx > 0)
                            {
                                Vector4 plane = depthmapPlanes[planeIdx];
                                float dist = Mathf.Abs(plane.w / (pos.x * plane.x + pos.y * plane.y + pos.z * plane.z));

                                if (writePseudoDepth)
                                    colorArray_pseudoDepth[(height - 1 - y) * width + x] =  TextureUtils.PseudoDepthMap(dist, maxDist);

                                if (writeGrayScale)
                                    colorArray_grayScale[(height - 1 - y) * width + x] = new Color(dist / maxDist, dist / maxDist, dist / maxDist);

                                if (writeTurboColor)
                                    colorArray_turboColor[(height - 1 - y) * width + x] = TextureUtils.TurboColorMap(1 - (dist / maxDist));

                            }

                            else
                            {
                                
                                if (writePseudoDepth)
                                    colorArray_pseudoDepth[(height - 1 - y) * width + x] = new Color(1, 1, 1, 1);

                                if (writeGrayScale)
                                    colorArray_grayScale[(height - 1 - y) * width + x] = new Color(1, 1, 1, 1);

                                if (writeTurboColor)
                                    colorArray_turboColor[(height - 1 - y) * width + x] = new Color(1, 1, 1, 1);
                            }
                        }
                    }
                    if (writePseudoDepth)
                    {
                        Texture2D depth_peudoDepth = new Texture2D(width, height, TextureFormat.RGBAFloat, false);
                        depth_peudoDepth.SetPixels(colorArray_pseudoDepth);
                        depth_peudoDepth.Apply();

                        byte[] bytes = depth_peudoDepth.EncodeToEXR();
                        File.WriteAllBytes(info_pseudoDepth.FullName, bytes);
                    }

                    if (writeGrayScale)
                    {
                        Texture2D depth_grayScale = new Texture2D(width, height, TextureFormat.RGBAFloat, false);
                        depth_grayScale.SetPixels(colorArray_grayScale);
                        depth_grayScale.Apply();

                        byte[] bytes = depth_grayScale.EncodeToEXR();
                        File.WriteAllBytes(info_grayScale.FullName, bytes);
                    }

                    if (writeTurboColor)
                    {
                        Texture2D depth_turboColor = new Texture2D(width, height, TextureFormat.RGBAFloat, false);
                        depth_turboColor.SetPixels(colorArray_turboColor);
                        depth_turboColor.Apply();

                        byte[] bytes = depth_turboColor.EncodeToPNG();
                        File.WriteAllBytes(info_turboColor.FullName, bytes);
                    }
                    progress = (float)++count / streetViewInfoList.Count;
                    yield return null;
                }
            }
            UnityEditor.AssetDatabase.Refresh();
            isComplete = true;
        }

        public IEnumerator LoadTextures(List<GameObject> imgObjList, TextureType textureType, int targetZoom = -1)
        {
            //CurrentState = "Loading images to the gameobjects...";
            totalCount = imgObjList.Count;

            for (int i = 0; i < imgObjList.Count; i++)
            {
                StreetViewItem svInfo = imgObjList[i].GetComponent<StreetViewItemMono>().streetviewInfo;

                int widthSize = 2048;

                string fullPath;
                Texture2D texture;

                if (textureType == TextureType.Color)
                {
                    switch (targetZoom)
                    {
                        case 1:
                            widthSize = 1024;
                            break;
                        case 2:
                            widthSize = 2048;
                            break;
                        case 3:
                            widthSize = 4096;
                            break;
                        case 4:
                        case 5:
                            widthSize = 8192;
                            break;
                    }
                    fullPath = ARRCPaths.GetColorImagePathFromStreetViewInfo(svInfo.folderName, targetZoom, svInfo.panoid + ".png");
                    string assetPath = ARRCPaths.GetAssetPath(fullPath);
                    texture = TextureUtils.LoadTextureUsingAssetDataBase(assetPath, widthSize);
                }

                else if (textureType == TextureType.Depth)
                {
                    fullPath = ARRCPaths.GetTurboColorPath(svInfo.folderName, svInfo.panoid + ".png");
                    string assetPath = ARRCPaths.GetAssetPath(fullPath);
                    texture = TextureUtils.LoadTextureUsingAssetDataBase(assetPath, widthSize);
                }

                else
                {
                    fullPath = ARRCPaths.GetLabelImagePath(svInfo.panoid + ".png");
                    texture = TextureUtils.LoadTexture(fullPath);
                }

                Renderer rend = imgObjList[i].GetComponent<MeshRenderer>();
                rend.sharedMaterial.mainTexture = texture;

                if (textureType == TextureType.Color)
                    svInfo.loadedZoom = targetZoom; //load가 된 후에야, 해당 streetview의 zoom을 갱신

                imgObjList[i].GetComponent<StreetViewItemMono>().loadedTextureType = textureType;
                imgObjList[i].GetComponent<StreetViewItemMono>().loadedTexturePath = fullPath;

                progress = (float)(i + 1) / imgObjList.Count;
                yield return null;
            }
            isComplete = true;
        }

        public IEnumerator ResetStreetViewObject(List<GameObject> targetImgObjList, List<StreetViewItem> streetViewInfoList)
        {
            if (targetImgObjList.Count != streetViewInfoList.Count)
                Debug.LogError("error");

            //CurrentState = "Loading images to the gameobjects...";
            totalCount = targetImgObjList.Count;

            for (int i = 0; i < targetImgObjList.Count; i++)
            {
                //reset streetviewinfo.
                targetImgObjList[i].GetComponent<StreetViewItemMono>().streetviewInfo = streetViewInfoList[i];

                progress = (float)(i + 1) / targetImgObjList.Count;
            }

            yield return null;
            isComplete = true;
        }

        public void AddPartImageDownloadList(StreetViewItem svInfo, int targetZoom, DownloadManager downloadManager)
        {
            //currentState = "Making download list for StreetView Images...";

            string subFolder_Image = ARRCPaths.CachFolder_StreetView_Image(targetZoom);
            if (!Directory.Exists(subFolder_Image)) Directory.CreateDirectory(subFolder_Image);

            svInfo.CalculateTiles(targetZoom, out int tile_num_X, out int tile_num_Y, out int tile_width_practical, out int tile_height_practical
                , out int tile_num_X_practical, out int tile_num_Y_practical);

            //svInfo.path_partImage = new Dictionary<int, string>();

            for (int y = 0; y < tile_num_Y_practical; y++)
            {
                for (int x = 0; x < tile_num_X_practical; x++)
                {
                    string url = string.Format(url_partImage, svInfo.panoid, targetZoom, x, y);
                    string filepath = GetStreetViewCacheFilePath(targetZoom, svInfo.panoid, x, y);

                    if (!File.Exists(filepath))
                    {
                        downloadManager.Add(new DownloadItemUnityWebRequest(url)
                        {
                            filename = filepath,
                            averageSize = ARRCDataSize.AVERAGE_STREETVIEW_SIZE
                        });
                    }
                }
            }
        }

        public StreetViewItem CreateStreetViewInfoFromFile(string filepath, string folderName)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(filepath);

            DictionaryOfStringAndString metadata = new DictionaryOfStringAndString();

            XmlNode fnode = doc.SelectSingleNode("panorama/data_properties");
            foreach (string property in StreetViewItem.properties_data)
                metadata.Add(property, XMLExt.GetAttribute<string>(fnode, property));

            fnode = doc.SelectSingleNode("panorama/projection_properties"); ;
            foreach (string property in StreetViewItem.properties_projection)
                metadata.Add(property, XMLExt.GetAttribute<string>(fnode, property));
            //throw new Exception("The reques is not validate for data projection properties.");
            //projection property.

            //text property.
            fnode = doc.SelectSingleNode("panorama/data_properties/text");
            if (fnode == null)
                metadata.Add("text", "");
            else
                metadata.Add("text", fnode.InnerText);

            //region property.
            fnode = doc.SelectSingleNode("panorama/data_properties/region");
            if (fnode == null)
                metadata.Add("region", "");
            else
                metadata.Add("region", fnode.InnerText);

            //country
            fnode = doc.SelectSingleNode("panorama/data_properties/country");
            if (fnode == null)
                metadata.Add("country", "");
            else
                metadata.Add("country", fnode.InnerText);

            //depth property.
            fnode = doc.SelectSingleNode("panorama/model/depth_map");
            if (fnode == null)
                metadata.Add("depth_map", "");

            else
                metadata.Add("depth_map", fnode.InnerText);


            return new StreetViewItem(metadata, folderName);
        }

        static GameObject CreateStreetViewObject(StreetViewItem svInfo, CoordinateInfo coordinateInfo)
        {
            double lat = double.Parse(svInfo.original_lat);
            double ele = double.Parse(svInfo.elevation_egm96_m);
            double lon = double.Parse(svInfo.original_lng);

            //Vector2 posXZ = CoordinateConverter.LatLonToWorldXZ(coordinateContainer, lat, lon);

            coordinateInfo.ConvertXY(lon, lat, out double outputX, out double outputY);
            Vector3 pos = new Vector3((float)(outputX - coordinateInfo.targetBoundary.minX), (float)ele, (float)(outputY - coordinateInfo.targetBoundary.minY));

            float yaw = float.Parse(svInfo.pano_yaw_deg);
            float pitch = float.Parse(svInfo.tilt_pitch_deg);

            string name = svInfo.panoid;

            GameObject newObj = CreateIcosahedronImageSphere(name, null/*texture*/, 5, 3, pos, Quaternion.Euler(0, 0, 0));
            //GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);

            newObj.transform.Rotate(new Vector3(0, yaw, 0), Space.World);
            newObj.transform.Rotate(new Vector3(pitch, 0, 0), Space.World);

            return newObj;
        }

        static GameObject CreateIcosahedronImageSphere(string filename, Texture2D tex, float radius, int subdivision, Vector3 pos, Quaternion rot, Transform parent = null, bool addGeoSphereComponent = false)
        {
            GameObject go = new GameObject();
            go.transform.position = pos;
            go.transform.rotation = rot;

            if (parent != null) go.transform.parent = parent;
            go.name = filename;

            //Renderer
            Renderer rend = go.AddComponent<MeshRenderer>();
            rend.material = new Material(Shader.Find("SphericalMapping/UnlitTexture"));

            //Mesh
            if (tex != null)
                rend.sharedMaterial.mainTexture = tex;


            if (addGeoSphereComponent)
            {
                go.AddComponent<MeshFilter>().sharedMesh = new Mesh(); //미리 shaderdMesh가 할당되어 있지 않으면 AddComponenet<GeoSphere>()에서 워닝이 발생함.
                go.AddComponent<GeoSphere>();
                go.GetComponent<GeoSphere>().baseType = PrimitivesPro.Primitives.GeoSpherePrimitive.BaseType.Icosahedron;
                go.GetComponent<GeoSphere>().pivotPosition = PrimitivesPro.Primitives.PivotPosition.Center;
                go.GetComponent<GeoSphere>().normalsType = PrimitivesPro.Primitives.NormalsType.Face;
                go.GetComponent<GeoSphere>().radius = radius;
                go.GetComponent<GeoSphere>().subdivision = subdivision;
                go.GetComponent<GeoSphere>().GenerateGeometry();
            }
            else
            {
                //MeshFilter 
                string assetfolder = @"Assets\Dependency\DTGenerator\Resources";
                string assetname = string.Format("sphere_subdiv{0}.asset", +subdivision);
                string assetpath = Path.Combine(assetfolder, assetname);
                Mesh mesh = (Mesh)AssetDatabase.LoadAssetAtPath(assetpath, typeof(Mesh));
                go.AddComponent<MeshFilter>().mesh = mesh;
            }

            return go;
        }

        static GameObject CreateCameraObject(Transform trans)
        {
            string name = "Pano Camera";
            GameObject newCam = new GameObject(name);
            newCam.transform.position = trans.position;
            newCam.transform.rotation = trans.rotation;
            newCam.AddComponent<Camera>();
            newCam.GetComponent<Camera>().clearFlags = CameraClearFlags.SolidColor;
            newCam.SetActive(false);
            return newCam;
        }


        static void DecompressData(byte[] inData, out byte[] outData)
        {
            using (MemoryStream outMemoryStream = new MemoryStream())
            using (ZOutputStream outZStream = new ZOutputStream(outMemoryStream))
            using (Stream inMemoryStream = new MemoryStream(inData))
            {
                CopyStream(inMemoryStream, outZStream);
                outZStream.finish();
                outData = outMemoryStream.ToArray();
            }
        }

        static void CopyStream(System.IO.Stream input, System.IO.Stream output)
        {
            byte[] buffer = new byte[2000];
            int len;
            while ((len = input.Read(buffer, 0, 2000)) > 0)
            {
                output.Write(buffer, 0, len);
            }
            output.Flush();
        }
        string GetStreetViewCacheFilePath(int targetZoom, string panoid, int x, int y)
        {
            string subFolder_Image = ARRCPaths.CachFolder_StreetView_Image(targetZoom);
            string filename = string.Format("{0}{1}_{2}.png", panoid, x.ToString(), y.ToString());
            string filepath = Path.Combine(subFolder_Image, filename);

            return filepath;
        }


    }
}
