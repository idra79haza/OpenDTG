﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.IO;
using System.Collections;
using UnityEditor;
using System.Linq;
using ARRC.Commons;
//using Proj4Net;

namespace ARRC.DigitalTwinGenerator
{
    public class VWorldBuildingGenerator : ARRCGenerator
    {
        const double R = 6378137;
        string title;

        string url3 = "http://xdworld.vworld.kr:8080/XDServer/requestLayerNode?APIKey=";
        string url4 = "http://xdworld.vworld.kr:8080/XDServer/requestLayerObject?APIKey=";
        string apiKey = "CEB52025-E065-364C-9DBA-44880E3B02B8";
        //string referer = "http://localhost:4141"; //apikey를 신청할 때 입력하는 호스트 주소

        //교량은 레벨 14에서 받아와야 한다.
        //static string layerName = "facility_bridge";
        //static int level = 14;

        //건물은 레벨 15에서 받아와야 한다.
        const string layerName = "facility_build";
        const int level = 15;

        //독도의 지형과 건물은 레벨 13에서 받아와야 한다. 독도의 지형 데이터는 DEM 쪽에 없고 여기에 함께 포함되어 있다.
        //static string layerName = "facility_dokdo";
        //static int level = 13;
        double unit = 360 / (Math.Pow(2, level) * 10); //15레벨의 격자 크기(단위:경위도)

        //중복 다운로드를 피하기 위해 현재 있는 파일들 목록을 구한다.
        HashSet<string> jpgList;
        HashSet<string> fileNamesXdo;

        //다운로드받은 xdo파일에 대한 filename, lat,lon, altitude 정보.
        Dictionary<string, VWorldBuildingItem> dicBuildingInfo;

        ArrayList idxIdyList;

        Material buildingMTL = new Material(Shader.Find("Standard"));

        public IEnumerator MakeDownloadItemList_XDODataList(string title, (double minLon, double minLat, double maxLon, double maxLat) wgs84Range, DownloadManager downloadManager)
        {
            //targetFolder += "/" + title;
            //CurrentState = "Making download list on XDO data...";

            string[] folders_in_cacheDir = { "xdo_dat", "xdo_Files", "xdo_List", "obj" };
            //string[] folders_in_targetDir = { "obj" };

            //if (!Directory.Exists(cacheFolder)) Directory.CreateDirectory(cacheFolder);
            foreach (string name in folders_in_cacheDir)
            {
                string subfolder = Path.Combine(ARRCPaths.CachFolder_VWorld, name);
                if (!Directory.Exists(subfolder))
                    Directory.CreateDirectory(subfolder);
            }

            //foreach (string name in folders_in_targetDir)
            //{
            //    string subfolder = Path.Combine(ARRCPaths.ResultFolder_Building(title), name);
            //    if (!Directory.Exists(subfolder))
            //        Directory.CreateDirectory(subfolder);
            //}

            //원래는 request와 response를 통해 idx idy 목록들을 받아와야 하지만, 간단한 계산을 통해 구할 수 있으므로 직접 한다.
            int minIdx = (int)Math.Floor((wgs84Range.minLon + 180) / unit);  //minLon
            int minIdy = (int)Math.Floor((wgs84Range.minLat + 90) / unit); //minLat
            int maxIdx = (int)Math.Floor((wgs84Range.maxLon + 180) / unit); //maxLon
            int maxIdy = (int)Math.Floor((wgs84Range.maxLat + 90) / unit); //maxLat

            int index = 0;
            idxIdyList = new ArrayList();

            for (int i = minIdx; i <= maxIdx; i++)
            {
                for (int j = minIdy; j <= maxIdy; j++)
                {
                    idxIdyList.Add(new Vector2(i, j));
                    index++;
                }
            }

            float total_progress = idxIdyList.Count;
            int count_progress = 0;

            foreach (Vector2 idxidy in idxIdyList)
            {
                int i = (int)idxidy.x;
                int j = (int)idxidy.y;

                string address3 = url3 + apiKey + "&Layer=" + layerName + "&Level=" + level
                + "&IDX=" + i.ToString() + "&IDY=" + j.ToString();
                string nameXdo = "xdoList" + i.ToString() + "_" + j.ToString() + ".dat";
                string filepathXdo = Path.Combine(ARRCPaths.CachFolder_VWorld, "xdo_dat/" + nameXdo);

                //Debug.Log("file :" + i.ToString() + "_" + j.ToString() + "세션 시작....." + (idx + 1).ToString());
                downloadManager.Add(new DownloadItemUnityWebRequest(address3)
                {
                    filename = filepathXdo,
                    averageSize = ARRCDataSize.AVERAGE_VWORLD_DAT
                });
                progress = (++count_progress) / total_progress;
                yield return new WaitForFixedUpdate();
            }
            isComplete = true;
        }


        //XDO data파일을 Parsing하여 .txt로 저장하고 이를 다시 로드하여 관련된 xdo파일들을 requeset한다.
        public IEnumerator MakeDownloadItemList_XDOData(DownloadManager downloadManager)
        {
            //CurrentState = "Making download list on XDO Data...";

            //저장될 정보들을 담을 Collection들을 초기화.

            dicBuildingInfo = new Dictionary<string, VWorldBuildingItem>();

            for (int i = 0; i < idxIdyList.Count; i++)
            {
                Vector2 idxidy = (Vector2)idxIdyList[i];
                string idx = idxidy.x.ToString();
                string idy = idxidy.y.ToString();

                string fileNameXdo = "xdoList" + idx + "_" + idy + ".dat";
                string filepathXdo = Path.Combine(ARRCPaths.CachFolder_VWorld, "xdo_dat/" + fileNameXdo);

                if (!CheckDat(filepathXdo))
                {
                    Debug.Log(filepathXdo + " : VWorld's Buliding : No data");
                    continue;
                }
                string nameParsedXdo = "xdoList_parsed" + idx + "_" + idy + ".txt";
                string filePathParsedXdo = Path.Combine(ARRCPaths.CachFolder_VWorld, "xdo_List/" + nameParsedXdo);

                //if(//check )
                datParser(filepathXdo, filePathParsedXdo);
                AddXDOtoDownloadItemList(filePathParsedXdo, idx, idy, downloadManager); //개별적인 xdo들을 호출하여 obj 파일로 만든다.			

                progress = (float)(i + 1) / idxIdyList.Count;
                yield return new WaitForFixedUpdate();
            }

            isComplete = true;
        }

        //다운로드 된 Xdo 파일들을 Parsing하여 Obj파일+Material파일로 변환한다.xdo에서 parsing된 jpg파일명을 이용해 texture파일을 request한다.
        public IEnumerator WriteOBJFiles(DownloadManager downloadManager)
        {
            //CurrentState = "Converting and Writing to .OBJ...";

            int idx = 0;
            foreach (KeyValuePair<string, VWorldBuildingItem> it in dicBuildingInfo)
            {
                string xdoFilePath = it.Key;
                VWorldBuildingItem binfo = it.Value;

                xdoParser_planer(xdoFilePath, binfo, downloadManager);

                progress = (float)++idx / dicBuildingInfo.Count;
                yield return new WaitForFixedUpdate();
            }

            isComplete = true;
        }

        public IEnumerator ImportModelstoAssets(string folderName)
        {
            //CurrentState = "Generating VWorld's 3D Models ...";
            int idx = 0;

            foreach (KeyValuePair<string, VWorldBuildingItem> it in dicBuildingInfo)
            {
                VWorldBuildingItem binfo = it.Value;

                string name = binfo.xdofileName_without_ext;
                string objName = name + ".obj";
                string mtlName = name + ".mtl";
                string texName = binfo.textureFileName_lod0;

                string cacheFilePath_obj = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", objName);
                string cacheFilePath_mtl = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", mtlName);
                string cacheFilePath_tex = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", texName);

                string resultFullPath_mesh = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), name + ".asset");
                string resultFullPath_material = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), mtlName);
                string resultFullPath_texture = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), texName);

                string assetpath_mesh = ARRCPaths.GetAssetPath(resultFullPath_mesh);
                string assetpath_material = ARRCPaths.GetAssetPath(resultFullPath_material);
                string assetpath_texture = ARRCPaths.GetAssetPath(resultFullPath_texture);

                Directory.CreateDirectory(ARRCPaths.ResultFolder_Building(folderName));

                if (File.Exists(cacheFilePath_obj) && !File.Exists(resultFullPath_mesh))
                {
                    //Load to sharedMemory. not recomended
                    GameObject temp_go = ObjectLoader.Load(ARRCPaths.CachFolder_VWorld + "/obj/", objName);
                    temp_go.hideFlags = HideFlags.HideAndDontSave;
                    Mesh orginalMesh = temp_go.GetComponent<MeshFilter>().sharedMesh;

                    //var newMesh = PrimitivesPro.MeshUtils.CopyMesh(orginalMesh);

                    AssetDatabase.CreateAsset(orginalMesh, assetpath_mesh);
                    AssetDatabase.SaveAssets();

                    UnityEngine.Object.DestroyImmediate(temp_go);
                }
                //if (File.Exists(cacheFilePath_mtl) && !File.Exists(resultFullPath_material))
                //    File.Copy(cacheFilePath_mtl, resultFullPath_material);

                if (File.Exists(cacheFilePath_tex) && !File.Exists(resultFullPath_texture))
                {
                    File.Copy(cacheFilePath_tex, resultFullPath_texture);
                    AssetDatabase.Refresh();

                }
                
                progress = (float)++idx / dicBuildingInfo.Count;
                yield return null;
            }


            isComplete = true;
        }

        public IEnumerator CreateBuilding(string folderName, CoordinateInfo coordinateInfo, bool useSharedMemory)
        {
            GameObject parent = new GameObject("Buildings");


            for(int i=0; i<dicBuildingInfo.Count; i++)
            {
                VWorldBuildingItem binfo = dicBuildingInfo.ElementAt(i).Value;

                string name = binfo.xdofileName_without_ext;
                //string mtlName = name + ".mtl";
                string texName = binfo.textureFileName_lod0;



                string resultFullPath_mesh = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), name + ".asset");
                //string resultFullPath_material = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), mtlName);
                string resultFullPath_texture = Path.Combine(ARRCPaths.ResultFolder_Building(folderName), texName);

                string assetpath_mesh = ARRCPaths.GetAssetPath(resultFullPath_mesh);
                //string assetpath_material = ARRCPaths.GetAssetPath(resultFullPath_material);
                string assetpath_texture = ARRCPaths.GetAssetPath(resultFullPath_texture);

                string cacheFolder_obj = Path.Combine(ARRCPaths.CachFolder_VWorld, "obj");
                string cacheFilePath_obj = Path.Combine(cacheFolder_obj, name+".obj");
                GameObject go;

                if (useSharedMemory && File.Exists(cacheFilePath_obj))
                {
                    go = ObjectLoader.Load(cacheFolder_obj, name + ".obj");
                }
                else if (!useSharedMemory && File.Exists(resultFullPath_mesh))
                {
                    go = new GameObject(name);
                    go.AddComponent<MeshFilter>().mesh = (Mesh)AssetDatabase.LoadAssetAtPath(assetpath_mesh, typeof(Mesh));
                    go.AddComponent<MeshRenderer>().material = new Material(buildingMTL);
                    go.GetComponent<MeshRenderer>().sharedMaterial.mainTexture = (Texture)AssetDatabase.LoadAssetAtPath(assetpath_texture, typeof(Texture));
                    //texture  없을 경우 예외처리 필요.
                }
                else
                {
                    Debug.LogWarning(resultFullPath_mesh + " is not exsit : Fail to generate gameobject");
                    continue;
                }
                go.transform.parent = parent.transform;
                VWorldBuildingItemMono compo = go.AddComponent<VWorldBuildingItemMono>();
                compo.VWorldBuildingItem = binfo;
                compo.coordinateInfo = coordinateInfo;

                coordinateInfo.ConvertXY(binfo.lon, binfo.lat, out double minX, out double minY);
                go.transform.position = new Vector3((float)(minX - coordinateInfo.targetBoundary.minX), (float)binfo.altitude, (float)(minY - coordinateInfo.targetBoundary.minY));

                int buildinglayer = LayerMask.NameToLayer("Building");
                if (buildinglayer == -1)
                    Debug.LogError("Building Layer must be assigned in Layer Manager.");
                else
                    go.layer = buildinglayer;

                progress = (float)(i+1) / dicBuildingInfo.Count;
                yield return null;
            }


            

            //parent.transform.position = new Vector3((float)-config.minX, 0, (float)-config.minY);
            isComplete = true;
        }

        private void AddXDOtoDownloadItemList(string filePathParsedXdo, string nodeIDX, string nodeIDY, DownloadManager downloadManager)
        {
            //읽기
            FileStream fr = new FileStream(filePathParsedXdo, FileMode.Open, FileAccess.Read);
            StreamReader br = new StreamReader(fr);

            string line;
            string[] temp;

            //네 줄은 파일목록이 아니므로 건너뛴다.
            br.ReadLine();
            br.ReadLine();
            br.ReadLine();
            br.ReadLine();

            //xdoList에서 xdo 파일이름을 하나하나 읽어들이면서 obj파일을 기록한다.
            //하나의 xdoList에 있는 건물들은 하나의 obj파일에 넣는다.
            while ((line = br.ReadLine()) != null)
            {
                temp = line.Split('|');
                string version = temp[0].Split('.')[3]; //Verison : XDO 파일의 버전 정보
                string xdofileName = temp[15];
                string key = temp[3]; //객체의 고유 ID 값
                double lon = double.Parse(temp[4]); //객체의 중심좌표(경위도 radian)
                double lat = double.Parse(temp[5]);  //객체의 중심좌표(경위도 radian)
                double altitude = double.Parse(temp[6]); //객체의 고도값
                double[] box = new double[6]; //객체의 바운더리 (구면 Vector) { min: {…}, max: {…} }

                for (int i = 0; i < 6; i++)
                    box[i] = double.Parse(temp[7 + i]);

                string url = getAddressForXdoFile(xdofileName, nodeIDX, nodeIDY);
                string filepath = Path.Combine(ARRCPaths.CachFolder_VWorld + "/xdo_Files", xdofileName);

                //저장될 xdo파일의 정보를 미리 저장한다.

                string xdofileName_without_ext = xdofileName.Substring(0, xdofileName.Length - 4); //확장자 제외.
                dicBuildingInfo.Add(filepath, new VWorldBuildingItem(key, xdofileName_without_ext, lat, lon, altitude, version, nodeIDX, nodeIDY, box));

                //다운로드리스트에 추가.
                downloadManager.Add(new DownloadItemUnityWebRequest(url)
                {
                    filename = filepath,
                    averageSize = ARRCDataSize.AVERAGE_VWORLD_XDO
                });

            }

            br.Close();
        }
        private void xdoParser_planer(string xdofilepath, VWorldBuildingItem binfo, DownloadManager downloadManager)
        {
            FileStream bis = new FileStream(xdofilepath, FileMode.Open, FileAccess.Read);

            //Type: 객체의 형식 Real3D Model은 8 고정으로 사용
            int type = VWorldXDOParsor.pU8(bis);
            if (type != 8)
            {
                Debug.LogError(xdofilepath + "The file not exist on server.");
                return;
            }

            //Object ID : 객체의 고유 ID 식별번호, 레이어 기준으로 객체 등록 번호
            int objectId = VWorldXDOParsor.pU32(bis);

            //Key : 객체의 고유 식별 문자열
            int keyLen = VWorldXDOParsor.pU8(bis);
            string key = VWorldXDOParsor.pChar(bis, keyLen);

            //Object Box : 객체의 최대 최소 영역 범위, World Wind 구면 Vector 좌표계
            double[] objectBox = { VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis) };

            //Altitude : 객체의 바닥면 기준 해발고도 높이
            float altitude = VWorldXDOParsor.pFloat(bis);

            double objX = (objectBox[0] + objectBox[3]) / 2;
            double objY = (objectBox[1] + objectBox[4]) / 2;
            double objZ = (objectBox[2] + objectBox[5]) / 2;

            //ConvertECEFToLatLon(objX, objY, objZ, out double lat, out double lon);
            //binfo.lat = lat;
            //binfo.lon = lon;

            ////double offset_Y = (objectBox[4] - objectBox[1]) / 2;
            //double originX = objxyz[0];
            //double originY = objxyz[1] - 6378137; //vworld이 참조하고 있는 world wind는 타원체가 아니라 6,378,137m의 반지름을 가지는 구면체다.
            //double originZ = objxyz[2];
            ////double originY = 0;

            if (binfo.version.Equals("1"))
            {
                CreateObjectFromXDO(bis, binfo, key, -1, downloadManager);
            }

            else
            {
                //FaceNum : 3D Mesh 배열의 총 수 지정
                int faceNum = VWorldXDOParsor.pU8(bis);

                if (binfo.xdofileName_without_ext.Equals("dongdo.xdo") && faceNum < 0) faceNum *= -1; //독도중 동도의 특별한 데이터 오류를 정정한다

                for (int j = 0; j < faceNum; j++)
                {
                    CreateObjectFromXDO(bis, binfo, key, j, downloadManager);
                }

            }


            bis.Close();

        }

        private void CreateObjectFromXDO(FileStream bis, VWorldBuildingItem binfo, string key, int faceIndex, DownloadManager downloadManager)
        {
            int nnP = 0;
            int vertexCount = VWorldXDOParsor.pU32(bis);

            //데이터 오류일 경우 건너뜀
            if (vertexCount <= 0)
            {
                Debug.LogError(binfo.xdofileName_without_ext + "Error : vertexCount <= 0");
                bis.Close();
                //return;
            }
            double[] objectBox = binfo.box;

            double objX = (objectBox[0] + objectBox[3]) / 2;
            double objY = (objectBox[1] + objectBox[4]) / 2;
            double objZ = (objectBox[2] + objectBox[5]) / 2;

            //ConvertECEFToLatLon(objX, objY, objZ, out double lat, out double lon);
            //binfo.lat = lat;
            //binfo.lon = lon;

            double[] objxyz = rotate3d((float)objX, (float)objY, (float)objZ, binfo.lon, binfo.lat);

            //double[] objxyz_top = rotate3d(objX, objectBox[1], objZ, binfo.lon, binfo.lat);
            //double[] objxyz_botttom = rotate3d(objX, objectBox[4], objZ, binfo.lon, binfo.lat);
            //double offsetY = (objxyz_top[1] - objxyz_botttom[1]) / 2;



            //coordinateInfo.ConvertXY(binfo.lon, binfo.lat, out double centerX, out double centerZ);
            //Vertex : 형상을 구성하는 정규화ii 된 공간 좌표점
            double[,] vertex = new double[vertexCount, 8];

            for (int i = 0; i < vertexCount; i++)
            {
                float vx = VWorldXDOParsor.pFloat(bis);
                float vy = VWorldXDOParsor.pFloat(bis);
                float vz = VWorldXDOParsor.pFloat(bis);
                float vnx = VWorldXDOParsor.pFloat(bis);
                float vny = VWorldXDOParsor.pFloat(bis);
                float vnz = VWorldXDOParsor.pFloat(bis);
                float vtu = VWorldXDOParsor.pFloat(bis);
                float vtv = VWorldXDOParsor.pFloat(bis);

                double[] xyz = rotate3d(vx, vy, vz, binfo.lon, binfo.lat);
                //vertex[i][0] = p2.x + xyz[0]; // p2는 lat, lon의 ecef 좌표값
                //vertex[i][1] = p2.y - 1 * (xyz[1]);
                //vertex[i][2] = xyz[2] + objxyz[2] - 6378137; 

                //vertex[i, 0] = vx;
                //vertex[i, 1] = vy; //왼손 좌표계 *-1
                //vertex[i, 2] = vz;

                //vertex[i, 0] = xyz[0] + centerX;
                //vertex[i, 1] = xyz[1] + objxyz[1] - R;
                //vertex[i, 2] = xyz[2] + centerZ;

                vertex[i, 0] = xyz[0];
                vertex[i, 1] = xyz[1] + objxyz[1] - R - binfo.altitude;
                vertex[i, 2] = xyz[2];

                vertex[i, 3] = vnx;
                vertex[i, 4] = vny;
                vertex[i, 5] = vnz;
                vertex[i, 6] = vtu;
                vertex[i, 7] = (1.0f - vtv);
            }

            int indexedNumber = VWorldXDOParsor.pU32(bis);

            short[] indexed = new short[indexedNumber];

            for (int i = 0; i < indexedNumber; i++)
            {
                indexed[i] = (short)(VWorldXDOParsor.pU16(bis) + 1);
            }

            //Color : Mesh의 재질의 색상 (A8R8G8B8)
            int colorA = VWorldXDOParsor.pU8(bis);
            int colorR = VWorldXDOParsor.pU8(bis);
            int colorG = VWorldXDOParsor.pU8(bis);
            int colorB = VWorldXDOParsor.pU8(bis);

            //ImageLevel : LOD 텍스쳐의 레벨 단계
            int imageLevel = VWorldXDOParsor.pU8(bis);
            int imageNameLen = VWorldXDOParsor.pU8(bis);

            //ImageName: LOD 텍스쳐 파일명
            string imageName = VWorldXDOParsor.pChar(bis, imageNameLen);

            //version2에서는 첫번째 face에 대한 lodName만을 저장해야한다. IJeon, 200423
            if (binfo.version.Equals("1") || (binfo.version.Equals("2") && faceIndex == 0))
                binfo.textureFileName_lod0 = imageName;

            int nailSize = VWorldXDOParsor.pU32(bis);
            //writeNailData(bis, imageName, nailSize);

            if (binfo.version.Equals("2"))
            {
                //이렇게 강제로 읽어서 해당 부분의 바이트를 소모해주지 않으면 다음 루프에서 읽어야 할 곳을 읽지 못해 에러가 난다.
                VWorldXDOParsor.writeNailData_fake(bis, imageName, nailSize);
            }

            string nodeIDX = binfo.nodeIDX;
            string nodeIDY = binfo.nodeIDY;
            string queryAddrForJpg = getAddressForJpgFile("", nodeIDX, nodeIDY);

            string url = queryAddrForJpg + imageName;
            downloadManager.Add(new DownloadItemUnityWebRequest(url)
            {
                filename = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", imageName),
                averageSize = ARRCDataSize.AVERAGE_VWORLD_TEXTURE
            });


            //다운받은 xdo파일을 파싱하여 vertex정보는 obj 파일로 쓰고, 다운받을 texture들을 downloadmanager에 등록한다.
            string objName = binfo.xdofileName_without_ext + ".obj";
            string mtlName = binfo.xdofileName_without_ext + ".mtl";

            string objFilePath = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", objName);
            string mtlFilePath = Path.Combine(ARRCPaths.CachFolder_VWorld + "/obj", mtlName);

            //Obj 기록
            FileStream fs = new FileStream(objFilePath, FileMode.Create, FileAccess.Write);
            StreamWriter bw = new StreamWriter(fs);

            //bw.WriteLine("# Rhino");
            bw.WriteLine();
            bw.WriteLine("mtllib " + mtlName);

            //Material 파일 기록
            FileStream fwm = new FileStream(mtlFilePath, FileMode.Create, FileAccess.Write);
            StreamWriter bwm = new StreamWriter(fwm);

            string keyName;
            if (binfo.version.Equals("1"))
                keyName = key;
            else
                keyName = key + "_" + faceIndex.ToString();

            bw.WriteLine("g " + keyName);

            //material의 기본적 속성은 임의로 아래와 같이 쓴다.
            //mtl 파일의 자세한 스펙은 아래를 참조
            //http://paulbourke.net/dataformats/mtl/
            mtlSubWriter(bwm, keyName, imageName);

            for (int i = 0; i < vertexCount; i++)
            {
                bw.WriteLine("v " + vertex[i, 0] + " " + vertex[i, 1] + " " + vertex[i, 2]);
            }
            for (int i = 0; i < vertexCount; i++)
            {
                bw.WriteLine("vt " + vertex[i, 6] + " " + vertex[i, 7]);
            }
            for (int i = 0; i < vertexCount; i++)
            {
                bw.WriteLine("vn " + vertex[i, 3] + " " + vertex[i, 4] + " " + vertex[i, 5]);
            }
            bw.WriteLine("usemtl " + keyName);
            for (int i = 0; i < indexedNumber; i = i + 3)
            {
                // unity의 left-handed 좌표계에 맞추기 위해 index[i+2]와 index[i+1]의 순서를 바꾸었다. byIB 19620
                bw.Write("f ");
                bw.Write((indexed[i] + nnP) + "/" + (indexed[i] + nnP) + "/" + (indexed[i] + nnP) + " ");
                bw.Write((indexed[i + 2] + nnP) + "/" + (indexed[i + 2] + nnP) + "/" + (indexed[i + 2] + nnP) + " ");
                bw.Write((indexed[i + 1] + nnP) + "/" + (indexed[i + 1] + nnP) + "/" + (indexed[i + 1] + nnP));
                bw.WriteLine();
            }

            bw.Close();
            bwm.Close();
        }

        private void mtlSubWriter(StreamWriter bw, string key, string imageName)
        {
            bw.WriteLine("newmtl " + key);
            bw.WriteLine("Ka 0.000000 0.000000 0.000000");
            bw.WriteLine("Kd 1.000000 1.000000 1.000000");
            bw.WriteLine("Ks 1.000000 1.000000 1.000000");
            bw.WriteLine("Tf 0.0000 0.0000 0.0000");
            bw.WriteLine("d 1.0000");
            bw.WriteLine("Ns 0");
            bw.WriteLine("map_Kd " + imageName);
            bw.WriteLine();
        }


        private string getAddressForXdoFile(string dataFile, string nodeIDX, string nodeIDY)
        {

            string address = url4 + apiKey + "&Layer=" + layerName + "&Level=" + level + "&IDX=" + nodeIDX + "&IDY=" + nodeIDY
                    + "&DataFile=" + dataFile;
            return address;
        }

        private string getAddressForJpgFile(string jpgFile, string nodeIDX, string nodeIDY)
        {
            string address = url4 + apiKey + "&Layer=" + layerName + "&Level=" + level + "&IDX=" + nodeIDX + "&IDY=" + nodeIDY
                    + "&DataFile=" + jpgFile;
            return address;
        }

        private static void datParser(string fileName, string fileNameW)
        {
            FileStream bis = new FileStream(fileName, FileMode.Open, FileAccess.Read);
            FileStream fs = new FileStream(fileNameW, FileMode.Create, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);


            int[] datHeader = new int[4];
            string[] datHeaderName = { "level", "IDX", "IDY", "ObjectCount" };

            //Header 읽기
            for (int i = 0; i < 4; i++)
            {
                datHeader[i] = VWorldXDOParsor.pU32(bis);
                sw.WriteLine(datHeaderName[i] + "=" + datHeader[i]);
            }


            //Real3D Model Object 읽기
            for (int i = 0; i < datHeader[3]; i++)
            {
                //Verison : XDO 파일의 버전 정보
                string r_version = VWorldXDOParsor.pU8(bis) + "." + VWorldXDOParsor.pU8(bis) + "." + VWorldXDOParsor.pU8(bis) + "." + VWorldXDOParsor.pU8(bis);

                //Type : 객체의 타입정보 – 8로 고정 (예약됨
                int r_type = VWorldXDOParsor.pU8(bis);

                //Key : 객체의 고유 ID 값
                int r_keylen = VWorldXDOParsor.pU8(bis);
                string r_key = VWorldXDOParsor.pChar(bis, r_keylen);

                //CenterPos : Model이 위치 할 공간좌표 경위도값
                double[] r_CenterPos = { VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis) };

                //Altitude : Model이 위치 할 공간의 해발고도 높이
                float r_altitude = VWorldXDOParsor.pFloat(bis);

                //Box : Model의 3차원 최대,최소 공간 영역 좌표 정보
                double[] r_box = { VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis), VWorldXDOParsor.pDouble(bis) };

                //ImgLevel : 사용되는 LOD 레벨의 수
                int r_imgLevel = VWorldXDOParsor.pU8(bis);

                //DataFile : Tile 내에 실제 Model의 XDO 파일명
                int r_dataFileLen = VWorldXDOParsor.pU8(bis);
                string r_dataFile = VWorldXDOParsor.pChar(bis, r_dataFileLen);

                //ImgFileName : XDO파일이 사용하는 Texture 이미지 명
                int r_imgFileNameLen = VWorldXDOParsor.pU8(bis);
                string r_imgFileName = VWorldXDOParsor.pChar(bis, r_imgFileNameLen);


                sw.WriteLine(r_version + "|" + r_type + "|" + r_keylen + "|" + r_key + "|" + r_CenterPos[0] + "|" + r_CenterPos[1]
                                + "|" + r_altitude + "|" + r_box[0] + "|" + r_box[1] + "|" + r_box[2] + "|" + r_box[3] + "|" + r_box[4] + "|" + r_box[5] + "|"
                                + r_imgLevel + "|" + r_dataFileLen + "|" + r_dataFile + "|" + r_imgFileNameLen + "|" + r_imgFileName);
            }

            sw.Close();
            bis.Close();
            fs.Close();
        }

        private static bool CheckDat(string fileNameXdo)
        {
            FileStream fs = new FileStream(fileNameXdo, FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);

            //첫줄에 해당 내용이 있다.
            string line = sr.ReadLine();
            sr.Close();
            int check = line.IndexOf("ERROR_SERVICE_FILE_NOTTHING");

            if (check == -1) return true;
            else return false;

        }

        private double[] rotate3d(double vx, double vy, double vz, double lon, double lat)
        {
            float x, y, z;

            double p = (lon) / 180 * Math.PI;
            double t = (90 - lat) / 180 * Math.PI;

            //원래 회전공식대로 하니까 90도 회전된 결과가 나와 z축을 중심으로 다시 -90도 회전을 했다.
            x = (float)(Math.Sin(p) * vx + Math.Cos(p) * vy);
            y = (float)(Math.Sin(t) * Math.Cos(p) * vx - Math.Sin(t) * Math.Sin(p) * vy + Math.Cos(t) * vz);
            z = -(float)(Math.Cos(t) * Math.Cos(p) * vx - Math.Cos(t) * Math.Sin(p) * vy - Math.Sin(t) * vz);

            // x = R * cos(lat) * cos(lon) 
            // y = R * cos(lat) * sin(lon) * -1(왼손좌표계) z = R * sin(lat)
            // z = R * sin(lat)

            return new double[] { x, y, z };
        }

        public void ConvertECEFToLatLon(double x, double y, double z, out double lat, out double lon)
        {
            lat = Math.Asin(z / R) * 180 / Math.PI;
            lon = Math.Atan2(y, x) * -1 * 180 / Math.PI;


        }


    }
}
