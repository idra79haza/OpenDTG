﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    public class VWorldBuildingItemMono : ARRCGeneratorMonoBase
    {
        [SerializeField]
        public VWorldBuildingItem VWorldBuildingItem;

        [SerializeField]
        public CoordinateInfo coordinateInfo;
    }
}