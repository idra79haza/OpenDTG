﻿using System;
using UnityEngine;

using Proj4Net;
using System.Collections.Generic;
using System.Linq;

namespace ARRC.DigitalTwinGenerator
{
    public class CoordinateInfo
    {
        public bool Init { get; private set; }

        CoordinateReferenceSystemFactory crsFactory;
        CoordinateReferenceSystem crs_source;
        CoordinateReferenceSystem crs_target;

        public string crsName_source, crsName_target;

        //boundary
        public (double minX, double minY, double maxX, double maxY) targetBoundary;
        //public (double minX, double minY, double maxX, double maxY) outputRange;


        //초기화 작업은 시간이 오래 걸리기 때문에 분리하였다.
        public void InitConverter(string crsName_source, string crsName_target)
        {
            this.crsName_source = crsName_source;
            this.crsName_target = crsName_target;

            crsFactory = new CoordinateReferenceSystemFactory();
            CreateCRS(ref crs_source, crsName_source);
            CreateCRS(ref crs_target, crsName_target);
            Init = true;
        }

        public void ConvertXY(double sourceX, double sourceY, out double targetX, out double targetY)
        {
            ProjCoordinate prjSource = new ProjCoordinate(sourceX, sourceY);
            ProjCoordinate prjTarget = new ProjCoordinate();

            BasicCoordinateTransform t1 = new BasicCoordinateTransform(crs_source, crs_target);
            t1.Transform(prjSource, prjTarget);

            targetX = prjTarget.X;
            targetY = prjTarget.Y;
        }


        void CreateCRS(ref CoordinateReferenceSystem crs, string crsName)
        {
            try
            {
                crs = crsFactory.CreateFromName(crsName);
            }

            catch (UnknownAuthorityCodeException ex)
            {
                if (CoordinateDB.CRSParameters.ContainsKey(ex.Message))
                {
                    crs = crsFactory.CreateFromParameters(ex.Message, CoordinateDB.CRSParameters[ex.Message]);
                }
                else
                {
                    Debug.LogException(ex);
                }
            }
        }

        public (double minX, double minY, double maxX, double maxY) ConvertRange((double minX, double minY, double maxX, double maxY) inputRange)
        {
            (double minX, double minY, double maxX, double maxY) outputRange  = inputRange;
            try
            {
                ConvertXY(inputRange.minX, inputRange.minY, out outputRange.minX, out outputRange.minY);
                ConvertXY(inputRange.maxX, inputRange.maxY, out outputRange.maxX, out outputRange.maxY);
                return outputRange;
            }
            catch (NullReferenceException ex)
            {
                Debug.LogException(ex);
            }
            return outputRange;
        }

        public void SetTargetBoundary((double minX, double minY, double maxX, double maxY) boundary)
        {
            targetBoundary =  ConvertRange(boundary);
        }

    }


}


