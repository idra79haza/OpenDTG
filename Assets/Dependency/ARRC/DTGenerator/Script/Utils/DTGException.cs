﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ARRC.DigitalTwinGenerator
{
    public class DTGException : Exception
    {
        public DTGException(string message) : base(message)
        {

        }
    }
    public class IDNotFoundException : DTGException
    {
        public IDNotFoundException(string message) : base(message)
        {

        }
    }

}
