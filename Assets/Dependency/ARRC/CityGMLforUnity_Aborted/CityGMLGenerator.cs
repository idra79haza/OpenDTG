﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Collections;
using System.Xml;
using CityGML2GO;
using CityGML2GO.GmlHandlers;
using System.Linq;
using System.Xml.Linq;
using ARRC.Commons;

namespace ARRC.DigitalTwinGenerator
{
    public class CityGMLGenerator : ARRCGenerator
    {
        string crsSource;

        // Use this for initialization
        public Vector3 Translate = Vector3.zero;
        public bool ShowDebug;
        public float UpdateRate;
        public bool ShowCurves;
        public bool Semantics;
        public float CurveThickness;
        public GameObject LineRendererPrefab;
        public bool GenerateColliders;
        public List<string> SemanticSurfaces = new List<string> { "GroundSurface", "WallSurface", "RoofSurface", "ClosureSurface", "CeilingSurface", "InteriorWallSurface", "FloorSurface", "OuterCeilingSurface", "OuterFloorSurface", "Door", "Window" };
        public Material DefaultMaterial;

        public List<Poly2Mesh.Polygon> oriPoly = new List<Poly2Mesh.Polygon>();
        public List<GameObject> Polygons = new List<GameObject>();
        public Dictionary<string, List<string>> Materials = new Dictionary<string, List<string>>();
        public List<TextureInformation> Textures = new List<TextureInformation>();

    

        public IEnumerator BuildGML(string title, string gmlFilePath, (double minX, double minY, double maxX, double maxY) inputRange_gml, CoordinateInfo convertor_gmlToTarget, bool useSharedMemory)
        {
            GameObject parent = new GameObject("Buildings");

            totalCount = XElement.Load(gmlFilePath)
               .Elements()
               .Where(e => e.Name.LocalName == "cityObjectMember")
               .Elements()
               .Where(e => e.Name.LocalName == "Building")
               .ToList().Count;


            using (XmlReader reader = XmlReader.Create(gmlFilePath, new XmlReaderSettings { IgnoreWhitespace = true }))
            {
                

                while (!reader.EOF)
                {
                    //모델이 나타날 때까지 읽기.
                    reader.Read();
                    if (reader.LocalName == "CityModel")
                    {
                        break;
                    }
                }

                var version = 0;
                for (int i = 0; i < reader.AttributeCount; i++)
                {
                    var attr = reader.GetAttribute(i);
                    if (attr == "http://www.opengis.net/citygml/1.0")
                    {
                        version = 1;
                        break;
                    }
                    if (attr == "http://www.opengis.net/citygml/2.0")
                    {
                        version = 2;
                        break;
                    }
                }

                if (version == 0)
                    Debug.LogWarning("Possibly invalid xml. Check for xml:ns citygml version.");

                
                float counter = 0;
                CityGml2GO param = new CityGml2GO(title, useSharedMemory, convertor_gmlToTarget);

                //Create folder.
                string  resultPath = Path.Combine(ARRCPaths.ResultFolder_Building(title));
                if (!Directory.Exists(resultPath)) Directory.CreateDirectory(resultPath);

                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "cityObjectMember")
                    {
                        while (reader.Read())
                        {
                            if (reader.NodeType == XmlNodeType.Element && reader.LocalName == "Building")
                            {
                                //counter++;
                                BuildingHandler.HandleBuilding(reader, param, parent.transform, this);

                                IXmlLineInfo xmlInfo = (IXmlLineInfo)reader;
                                int lineNumber = xmlInfo.LineNumber;
                                progress = ++counter / totalCount;

                                yield return null;
                            }
                        }
                    }
                }
            }
            MaterialHandler.ApplyMaterials(this);

            ChangeLayersRecursively(parent.transform, "Building");
            isComplete = true;
        }


        
        void ChangeLayersRecursively(Transform trans, string name)
        {
            trans.gameObject.layer = LayerMask.NameToLayer(name);
            foreach (Transform child in trans)
            {
                ChangeLayersRecursively(child, name);
            }
        }
  

    }


}
