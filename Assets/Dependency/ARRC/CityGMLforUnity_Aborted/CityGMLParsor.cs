﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using UnityEditor;
using UnityEngine;


public static class CityGMLParsor
{
    /// <summary>
    /// Raw string data from GML
    /// </summary>

    public static CityGMLModel CreateCityGMLModel(string gmlFilePath)
    {
        var builder_cityGMLModel = new CityGMLModel.Builder();

        var root = XElement.Load(gmlFilePath);

        DateTime time = DateTime.Now;

        //parsing version.
        var iVersion = root.Attributes()
            .Where(e => e.Value == "http://www.opengis.net/citygml/1.0" || e.Value == "http://www.opengis.net/citygml/2.0")
            .Select(e => e.Value);

        if (iVersion.Count() > 0) builder_cityGMLModel.SetVersion(iVersion.ToArray()[0]);


        //parsing global coordinate.
        var xele_envelope = root.Descendants()
            .Where(e => e.Name.LocalName == "boundedBy")
            .Descendants()
            .Where(e => e.Name.LocalName == "Envelope");

        var iepsg = xele_envelope.Select(e => e.Attribute("srsName").Value);

        if (iepsg.Count() > 0)
            builder_cityGMLModel.SetEPSG(ParseEPSG(iepsg.ToArray()[0]));

        var iLowerCorner = xele_envelope.Descendants()
                .Where(e => e.Name.LocalName == "lowerCorner")
                .Select(e => e.Value);

        if (iLowerCorner.Count() > 0)
        {
            double[] lowerCorner = iLowerCorner.ToList()[0].Split(' ').Select(e => double.Parse(e)).ToArray();
            builder_cityGMLModel.SetLowerCorner(lowerCorner);
        }


        var iUpperCorner = xele_envelope.Descendants()
             .Where(e => e.Name.LocalName == "upperCorner")
             .Select(e => e.Value);

        if (iUpperCorner.Count() > 0)
        {
            double[] upperCorner = iUpperCorner.ToList()[0].Split(' ').Select(e => double.Parse(e)).ToArray();
            builder_cityGMLModel.SetUpperCorner(upperCorner);
        }

        //Debug.Assert(builder_cityGMLModel.lowerCorner.Length == 3);
        //Debug.Assert(builder_cityGMLModel.upperCorner.Length == 3);
        //Debug.Log("version " + builder_cityGMLModel.version);

        //xelements of parse building.
        var xele_buildings = root.Descendants()
            .Where(e => e.Name.LocalName == "cityObjectMember")
            .Descendants()
            .Where(e => e.Name.LocalName == "Building");

        var buildings = new List<GMLBuilding>();

        foreach (var xele_bldg in xele_buildings)
        {
            var buildingID = xele_bldg.Attributes().Where(e => e.Name.LocalName == "id").Select(e => e.Value).ToList();
            var xele_polygon = xele_bldg.Descendants().Where(e => e.Name.LocalName == "Polygon");

            var polygons = new List<GMLPolygon>();

            foreach (var xele_plg in xele_polygon)
            {
                var polygonID = xele_plg.Attributes().Where(e => e.Name.LocalName == "id").Select(e => e.Value).ToList();

                var exterior = xele_plg.Descendants().Where(e => e.Name.LocalName == "exterior")
                     .Descendants().Where(e => e.Name.LocalName == "posList").Select(e => e.Value.Trim('\n', ' ').Split(' ').ToList()).ToList();

                var interior = xele_plg.Descendants().Where(e => e.Name.LocalName == "interior")
                     .Descendants().Where(e => e.Name.LocalName == "posList").Select(e => e.Value.Trim('\n', ' ').Split(' ').ToList()).ToList();

                GMLPolygon.Builder builder_plg = new GMLPolygon.Builder();

                if (polygonID.Count > 0) builder_plg.SetID(polygonID[0]);

                try
                {
                    if (exterior.Count > 0)
                    {
                        builder_plg.SetExterior(exterior[0].Select(e => double.Parse(e)).ToList());
                    }
                    if (interior.Count > 0)
                    {
                        builder_plg.SetInterior(interior[0].Select(e => double.Parse(e)).ToList());
                    }
                    polygons.Add(builder_plg.Build());
                }
                catch (FormatException ex)
                {
                    Debug.LogException(ex);
                    continue;
                }
            }

            GMLBuilding.Builder builder_bldg = new GMLBuilding.Builder();
            if (buildingID.Count > 0) builder_bldg.SetID(buildingID[0]);
            builder_bldg.SetPolygons(polygons);
            buildings.Add(builder_bldg.Build());
        }

        builder_cityGMLModel.SetBuildings(buildings);

        Debug.Log((DateTime.Now - time).TotalSeconds);

        return builder_cityGMLModel.Build();
    }



    // 사용중인 좌표계 받아오기.

    public static void GetCoordinateInfo(string gmlFilePath, ref string out_CRSCode, ref (double x, double y, double z) out_lowerCorner, ref (double x, double y, double z) out_upperCorner)
    {
        var root = XElement.Load(gmlFilePath);

        //parsing global coordinate.
        var xele_envelope = root.Descendants()
            .Where(e => e.Name.LocalName == "boundedBy")
            .Descendants()
            .Where(e => e.Name.LocalName == "Envelope");

        var iepsg = xele_envelope.Select(e => e.Attribute("srsName").Value);

        if (iepsg.Count() > 0)
        {
            out_CRSCode = ParseEPSG(iepsg.ToArray()[0]);
        }
        else
            throw new Exception("Not found coordinate info in the file.");

        var iLowerCorner = xele_envelope.Descendants()
                .Where(e => e.Name.LocalName == "lowerCorner")
                .Select(e => e.Value);

        if (iLowerCorner.Count() > 0)
        {
            double[] lowerCorner = iLowerCorner.ToList()[0].Split(' ').Select(e => double.Parse(e)).ToArray();
            out_lowerCorner = (lowerCorner[0], lowerCorner[2], lowerCorner[1]);
        }
        else
            throw new Exception("Not found coordinate info in the file.");


        var iUpperCorner = xele_envelope.Descendants()
             .Where(e => e.Name.LocalName == "upperCorner")
             .Select(e => e.Value);

        if (iUpperCorner.Count() > 0)
        {
            double[] upperCorner = iUpperCorner.ToList()[0].Split(' ').Select(e => double.Parse(e)).ToArray();
            out_upperCorner = (upperCorner[0], upperCorner[2], upperCorner[1]);
        }
        else
            throw new Exception("Not found coordinate info in the file.");


    }

    public static string ParseEPSG(string line)
    {
        //buildings
        string pattern_epgs = "EPSG:+[0-9]{3,9}";

        Regex regex = new Regex(pattern_epgs);
        string crsName = regex.Match(line).Value.Replace("::", ":");
        return crsName;
    }
}


