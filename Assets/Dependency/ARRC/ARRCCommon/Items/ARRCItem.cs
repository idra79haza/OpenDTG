﻿using System;
using UnityEngine;

namespace ARRC.Commons
{
    public class ARRCItem
    {
        public string guid;
        public long timestamp;

        public ARRCItem()
        {
            guid = Guid.NewGuid().ToString();
            timestamp = DateTime.Now.Ticks;
        }

    }
}
