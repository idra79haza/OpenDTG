﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using ARRC.DigitalTwinGenerator;
using System.IO;

namespace ARRC.StreetViewPoseOptimizer
{
    public enum ResolutionUnit { res256x128, res512x256, res1024x512 };
    public enum CubemapWidthUnit { res128, res256, res512 };

    public class StreetViewPoseOptimizerWindow : EditorWindow
    {
        StreetViewPoseOptimizerTaskManager taskManager = StreetViewPoseOptimizerTaskManager.Instance;

        [SerializeField]
        public List<GameObject> targetStreetViewList;
        List<GameObject> targetStreetViewList_backup;

        //PSO configuration.
        public int swarmSize = 120;
        public int iteration = 10;
        public double threshold = 0.001f;
        bool halt_iteration = true;
        bool halt_threshold = false;

        //Camera Setting for generating hypothsis.
        //public Resolution resolutions;
        public HypothesisType hypothesisType = HypothesisType.OutlinePixels;
        public ResolutionUnit resolutionUnit = ResolutionUnit.res1024x512;
        public CubemapWidthUnit cubemapWidthUnit = CubemapWidthUnit.res512;

        int equirectangularWidth;
        int equirectangularHeight;
        int cubemapWidth = 512;

        public LayerMask cameraLayerMask = 1 << 8; //building
        public float cameraNearPlane = 0.3f;
        public float cameraFarPlane = 400f;

        public string path_resultFolder;

        bool saveResult = false;

        float[] time_for_one = new float[] { 112, };
        Vector2 scrollPos = Vector2.zero;

        [MenuItem("ARRC/StreetView Optimizer/StreetView Optimizer")]
        public static StreetViewPoseOptimizerWindow OpenWindow()
        {
            return Instance;
        }

        void Awake()
        {
            UpdateTargetStreetViewList();
        }

        void OnGUI()
        {
            if (taskManager.IsEmpty)
            {
                scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
                //Debug.Log(Event.current.type.ToString());
                EditorGUILayout.BeginVertical(GUI.skin.box);
                ScriptableObject target = this;
                SerializedObject serializedObject = new SerializedObject(target);
                EditorGUILayout.PropertyField(serializedObject.FindProperty("targetStreetViewList"), true); // True means show children
                serializedObject.ApplyModifiedProperties(); // Remember to apply modified properties
                serializedObject.Update();
                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                swarmSize = EditorGUILayout.IntField("Swarm size", swarmSize);
                iteration = EditorGUILayout.IntField("Iteration", iteration);
                threshold = EditorGUILayout.DoubleField("Goal", threshold);

                halt_iteration = EditorGUILayout.Toggle("Halt_Iteration", halt_iteration);
                halt_threshold = EditorGUILayout.Toggle("Halt_Threshold", halt_threshold);

                hypothesisType = (HypothesisType)EditorGUILayout.EnumPopup("CostFunction", hypothesisType);
                resolutionUnit = (ResolutionUnit)EditorGUILayout.EnumPopup("Resoution", resolutionUnit);

                EditorGUILayout.EndVertical();

                EditorGUILayout.BeginVertical(GUI.skin.box);
                saveResult = EditorGUILayout.Toggle("Save result", saveResult);

                if (saveResult)
                {
                    if (GUILayout.Button("Select folder"))
                    {
                        //EditorUtility.DisplayDialog("Select Texture", "You must select a texture first!", "OK");
                        string path = EditorUtility.OpenFolderPanel("Select folder", Application.dataPath + "/../", "");
                        //int LastIndex = path.LastIndexOf ("/");
                        //mTotalPath = path;
                        path_resultFolder = path;

                    }
                    EditorGUILayout.LabelField("Result folder : " + path_resultFolder, EditorStyles.boldLabel);
                }
                EditorGUILayout.EndVertical();
                GUILayout.EndScrollView();
                if (GUILayout.Button("Capture"))
                {
                    SetResoultion(resolutionUnit, cubemapWidthUnit);

                    foreach (GameObject targetObject in targetStreetViewList)
                    {
                        var result = StreetViewPoseOptimizer.GetOverayTexture(targetObject, equirectangularWidth, equirectangularHeight, cameraNearPlane, cameraFarPlane, true);
                        DestroyImmediate(result);
                    }
                }

                if (GUILayout.Button("Compute cost"))
                {
                    foreach (GameObject targetObject in targetStreetViewList)
                    {
                        SetResoultion(resolutionUnit, cubemapWidthUnit);
                        StreetViewPoseOptimizer.ComputeCost(targetObject, equirectangularWidth, equirectangularHeight, cubemapWidth, hypothesisType);
                    }
                }

                if (GUILayout.Button("Optimize"))
                {
                    StartOptimize();
                }

            }

            //EditorGUILayout.EndVertical();

            else
                OnOptimize();


        }

        void OnSelectionChange()
        {
            UpdateTargetStreetViewList();
        }

        void Update()
        {
            taskManager.Update();
        }

        void OnOptimize()
        {
            float progress = taskManager.ProgressOfActiveTask;
            float timePer = taskManager.ProcessingTimeOfActiveTask;
            float totalSize = taskManager.SizeOfActiveTask;
            int completed = Mathf.FloorToInt(totalSize * progress);
            float remainingTime = (1 - progress) * taskManager.SizeOfActiveTask * timePer;

            GUILayout.Label(taskManager.StateOfActiveTask + " (" + completed + " of " + totalSize + ")");
            TimeSpan time = TimeSpan.FromSeconds(remainingTime);
            GUILayout.Label("Remaining time : " + time.ToString("hh'h'mm'm'ss's'"));

            Rect r = EditorGUILayout.BeginVertical();
            string strProgress = Mathf.FloorToInt(progress * 100f).ToString();
            EditorGUI.ProgressBar(r, progress, strProgress + "%");
            GUILayout.Space(16);
            EditorGUILayout.EndVertical();


            if (!taskManager.IsCompleted && GUILayout.Button("Cancel"))
            {
                taskManager.Dispose();
            }

            if (taskManager.IsCompleted && GUILayout.Button("Done"))
            {
                taskManager.Dispose();
            }

            GUILayout.Label("Warning: Keep this window open.");
            Repaint();

        }

        void Dispose()
        {
            taskManager.Dispose();
            EditorUtility.UnloadUnusedAssetsImmediate();
        }
        void OnDestroy()
        {
            Dispose();
            instance = null;
        }

        void StartOptimize()
        {
            if (targetStreetViewList == null || targetStreetViewList.Count == 0)
            {
                Debug.Log("Select target objects.");
                return;
            }

            targetStreetViewList_backup = new List<GameObject>();

            foreach (GameObject go in targetStreetViewList)
                targetStreetViewList_backup.Add(go);

            SetResoultion(resolutionUnit, cubemapWidthUnit);

            taskManager = new StreetViewPoseOptimizerTaskManager();

            taskManager.Init(targetStreetViewList, equirectangularWidth, equirectangularHeight, cubemapWidth, cameraNearPlane, cameraFarPlane,
                swarmSize, iteration, threshold, halt_iteration, halt_threshold, hypothesisType,
                true, path_resultFolder, this);
        }
        void SetResoultion(ResolutionUnit _resolutionUnit, CubemapWidthUnit _cubemapWidthUnit)
        {
            if (_resolutionUnit == ResolutionUnit.res256x128)
            {
                equirectangularWidth = 256;
                equirectangularHeight = 128;
            }
            else if (_resolutionUnit == ResolutionUnit.res512x256)
            {
                equirectangularWidth = 512;
                equirectangularHeight = 256;
            }
            else if (_resolutionUnit == ResolutionUnit.res1024x512)
            {
                equirectangularWidth = 1024;
                equirectangularHeight = 512;
            }

            if (_cubemapWidthUnit == CubemapWidthUnit.res128)
            {
                cubemapWidth = 128;
            }
            else if (_cubemapWidthUnit == CubemapWidthUnit.res256)
            {
                cubemapWidth = 256;
            }
            else if (_cubemapWidthUnit == CubemapWidthUnit.res512)
            {
                cubemapWidth = 512;
            }
        }

        static StreetViewPoseOptimizerWindow instance;

        public static StreetViewPoseOptimizerWindow Instance
        {
            get
            {
                if (instance)
                    return instance;
                else
                {
                    instance = GetWindow<StreetViewPoseOptimizerWindow>(false, "StreetView Pose Optimizer");
                    return instance;
                }
            }
        }

        void UpdateTargetStreetViewList()
        {
            var filteredList = new List<GameObject>();

            if (Selection.gameObjects.Length > 0)
            {
                foreach (GameObject candidate in Selection.gameObjects)
                {
                    var compo = candidate.transform.GetComponent<StreetViewItemMono>();
                    if (compo != null)
                        filteredList.Add(candidate);
                }

                targetStreetViewList = filteredList;
                Repaint();
            }
        }



    }
}