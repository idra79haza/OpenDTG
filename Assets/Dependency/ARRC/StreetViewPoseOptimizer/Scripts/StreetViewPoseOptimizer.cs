﻿using System;
using System.IO;
using UnityEditor;
using UnityEngine;
using PSO;
using System.Collections;
using System.Runtime.InteropServices;
using ARRC.ARRCTexture;
using ARRC.Commons;
using ARRC.DigitalTwinGenerator;
using System.Collections.Generic;

public enum HypothesisType { AllPixels, OutlinePixels };

namespace ARRC.StreetViewPoseOptimizer
{
    enum ComputeShaderName
    {
        Diffrentiate,
        Diffrentiate_Outline,
        SumXYPlane,
        SumXYPlaneFinal256,
        SumXYPlaneFinal512,
        SumXYPlaneFinal1024,
    }

    public class StreetViewPoseOptimizer : ARRCGenerator
    {

        HypothesisType hypothesisType = HypothesisType.OutlinePixels;

        //Camera and Image setting for generating hypothsis.
        GameObject global_captureCamera;
        float cameraNearPlane = 0.3f;
        float cameraFarPlane = 200f;
        LayerMask cameraLayerMask = 1 << 8; //building

        int equirectangularWidth;  // must be pow of 2.
        int equirectangularHeight; // must be pow of 2.
        int cubemapWidth;

        const int groupDimX = 32;
        const int groupDimY = 32;

        int gridDimX;
        int gridDimY;
        int gridDimZ;

        int kernel_diff, kernel_sum, kernel_sum2;
        ComputeShader computeShader_diff, computeShader_sum, computeShader_sum2;

        ////## Should be released manually.##
        ///
        ComputeBuffer buffer_diffMap, buffer_evalMap_after_reduction1, buffer_eval;
        Texture2D observation;
        RenderTexture diffTexture;

        int[] data_diffMap;
        int[] data_evalMap_after_reduction1;
        int[] data_eval;
        ///
        ///## Should be released manually.##

        //Vector3 beforeOpt_position;
        //Quaternion beforeOpt_rotation;

        public StreetViewPoseOptimizer(int equirectangularWidth, int equirectangularHeight, int cubemapWidth, HypothesisType hypothesisType)
        {
            //global_captureCamera = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask, cameraNearPlane, cameraFarPlane, -1, Color.black);
            this.equirectangularWidth = equirectangularWidth;
            this.equirectangularHeight = equirectangularHeight;
            this.cubemapWidth = cubemapWidth;

            //cubemapWidth = _cubemapWidth;
            this.hypothesisType = hypothesisType;

            //Init Computeshaders and buffers.
            InitComputeShaderandBuffers(equirectangularWidth, equirectangularHeight);
        }
        public override void Dispose()
        {
            //releaes computer buffers
            buffer_diffMap.Dispose();
            buffer_evalMap_after_reduction1.Dispose();
            buffer_eval.Dispose();

            //release texture
            UnityEngine.Object.DestroyImmediate(observation);
            diffTexture.Release();

            data_diffMap = null;
            data_evalMap_after_reduction1 = null;
            data_eval = null;

            GC.Collect();
        }

        public static void WriteOverayTexture(GameObject target, int equirectangularWidth, int equirectangularHeight, float cameraNearPlane, float cameraFarPlane
            , string saveFilePath, bool showWindow = false)
        {
            Texture2D result = GetOverayTexture(target, equirectangularWidth, equirectangularHeight, cameraNearPlane, cameraFarPlane, showWindow);
            TextureUtils.WriteTexture(result, saveFilePath);

            UnityEngine.Object.DestroyImmediate(result);
        }
        public static Texture2D GetOverayTexture(GameObject target, int equirectangularWidth, int equirectangularHeight, float cameraNearPlane, float cameraFarPlane, bool showWindow = false)
        {
            var streetViewItem = target.GetComponent<StreetViewItemMono>().streetviewInfo;
            if (streetViewItem == null)
                throw new Exception("The GameObject is not contains \"StreetViewItem\" Component.");

            string fullpath_image = ARRCPaths.GetColorImagePathFromStreetViewInfo(streetViewItem.folderName, streetViewItem.loadedZoom, streetViewItem.panoid + ".png");
            string fullpath_label = ARRCPaths.GetLabelImagePath(streetViewItem.panoid + ".png");
            int cameraLayerMask_building = (1 << LayerMask.NameToLayer("Building"));

            GameObject cam_building = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask_building, cameraNearPlane, cameraFarPlane, -1, Color.black, target.transform);
            Texture2D result = EquirectangularGenerator.GetOverayTexture(equirectangularWidth, equirectangularHeight, 512, cam_building, fullpath_image, fullpath_label, showWindow);
            UnityEngine.Object.DestroyImmediate(cam_building);
            return result;
        }

        public static double ComputeCost(GameObject target, int equirectangularWidth, int equirectangularHeight,int cubemapWidth, HypothesisType hypothesisType)
        {
            var streetViewItem = target.GetComponent<StreetViewItemMono>().streetviewInfo;
            if (streetViewItem == null)
                throw new Exception("The GameObject is not contains \"StreetViewItem\" Component.");

            string fullpath_label = ARRCPaths.GetLabelImagePath(streetViewItem.panoid + ".png");

            var optimizer = new StreetViewPoseOptimizer(equirectangularWidth, equirectangularHeight, cubemapWidth, hypothesisType);
            double cost = optimizer.ComputeCost(target.transform, fullpath_label);
            optimizer.Dispose();
            return cost;
        }
        public IEnumerator Optimize(List<GameObject> targetList, int equirectangularWidth, int equirectangularHeight, float cameraNearPlane, float cameraFarPlane, 
            int swarmSize, int iteration, double threshold, bool halt_iteration, bool halt_threshold, bool showResultWindow = false, string path_resultFolder = null)
        {
            float startTime = Time.realtimeSinceStartup;

            //currentState = "Optimizing transformation of streetviews...";

            totalCount = targetList.Count * iteration;

            int count = 0;

            for (int targetIdx = 0; targetIdx < targetList.Count; targetIdx++)
            {
                GameObject target = targetList[targetIdx];
                Selection.activeObject = target;

                var streetViewItem = target.GetComponent<StreetViewItemMono>().streetviewInfo;
                if (streetViewItem == null)
                    throw new Exception("The GameObject is not contains \"StreetViewItem\" Component.");

                if (!string.IsNullOrEmpty(path_resultFolder))
                {
                    string resultFilename = string.Format("{0:D4}_before_{1}.png", targetIdx, streetViewItem.panoid);
                    string resultFilePath = Path.Combine(path_resultFolder, resultFilename);
                    WriteOverayTexture(target, equirectangularWidth, equirectangularHeight, cameraNearPlane, cameraFarPlane, resultFilePath, showResultWindow);
                }
                string fullpath_label = ARRCPaths.GetLabelImagePath(streetViewItem.panoid + ".png");

                //Cretae hypothsis camera.
                global_captureCamera = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask, cameraNearPlane, cameraFarPlane, -1, Color.black, target.transform);

                //Set Observation.
                SetObservation(fullpath_label);

                //Init PSO.
                InitPSO(swarmSize, iteration, threshold, halt_iteration, halt_threshold, ConvertToArray(target.transform));

                for (int iter = 0; iter < iteration; iter++)
                {
                    float startTime_iter = Time.realtimeSinceStartup;

                    RESULT result = ParticleSwarmOptimizationWarpper.Instance.MoveOneStep(iter);
                    Debug.Log("Iteration " + (iter+1).ToString() + " : " + result.gBest_Err.ToString());

                    double[] currBest = new double[6];
                    Marshal.Copy(result.gBest, currBest, 0, currBest.Length);

                    target.transform.position = new Vector3((float)currBest[0], (float)currBest[1], (float)currBest[2]);
                    target.transform.rotation = Quaternion.Euler((float)currBest[3], (float)currBest[4], (float)currBest[5]);

                    if (Convert.ToBoolean(halt_iteration) && iter == iteration)
                        break;

                    //halt if error is low than threshold.
                    if (Convert.ToBoolean(halt_threshold) && (result.gBest_Err < threshold))
                        break;

                    progress = ++count / (float)totalCount;
                    float time = (Time.realtimeSinceStartup - startTime_iter);
                    UpdateProcssingTime(time);

                    yield return null;
                }
                //destroy camera
                UnityEngine.Object.DestroyImmediate(global_captureCamera);

                if (!string.IsNullOrEmpty(path_resultFolder))
                {
                    string resultFilename = string.Format("{0:D4}_after_{1}.png", targetIdx, streetViewItem.panoid);
                    string resultFilePath = Path.Combine(path_resultFolder, resultFilename);
                    WriteOverayTexture(target, equirectangularWidth, equirectangularHeight, cameraNearPlane, cameraFarPlane, resultFilePath, showResultWindow);
                }

            }
            Debug.Log("Time to optimize : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");
            isComplete = true;
        }
        public IEnumerator Optimize(Transform targetPose, string labelImagePath, int swarmSize, int iteration, double threshold, bool halt_iteration, bool halt_threshold, int index, int totalSize)
        {

            //GameObject target = targetList[targetIdx];

            //Cretae hypothsis camera.
            global_captureCamera = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask, cameraNearPlane, cameraFarPlane, -1, Color.black, targetPose);

            //Set Observation.
            //string filename_with_ext = target.GetComponent<StreetViewItemMono>().streetviewInfo.panoid + ".png";

            SetObservation(labelImagePath);

            //Init PSO.
            InitPSO(swarmSize, iteration, threshold, halt_iteration, halt_threshold, ConvertToArray(targetPose));

            for (int iter = 0; iter < iteration; iter++)
            {
                float startTime_iter = Time.realtimeSinceStartup;

                RESULT result = ParticleSwarmOptimizationWarpper.Instance.MoveOneStep(iter);
                Debug.Log("Iteration " + iter.ToString() + " : " + result.gBest_Err.ToString());

                double[] currBest = new double[6];
                Marshal.Copy(result.gBest, currBest, 0, currBest.Length);

                targetPose.position = new Vector3((float)currBest[0], (float)currBest[1], (float)currBest[2]);
                targetPose.transform.rotation = Quaternion.Euler((float)currBest[3], (float)currBest[4], (float)currBest[5]);

                if (Convert.ToBoolean(halt_iteration) && iter == iteration)
                    break;

                //halt if error is low than threshold.
                if (Convert.ToBoolean(halt_threshold) && (result.gBest_Err < threshold))
                    break;

                progress = (float)((index * iteration) + (iter + 1)) / totalSize * iteration;
                //float time = (Time.realtimeSinceStartup - startTime_iter);
                //UpdateProcssingTime(time);

                yield return null;
            }
            //destroy camera
            UnityEngine.Object.DestroyImmediate(global_captureCamera);

        }

        public double ComputeCost(Transform target, string labelFilePath)
        {
            float startTime = Time.realtimeSinceStartup;

            //Cretae hypothsis camera.
            global_captureCamera = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask, cameraNearPlane, cameraFarPlane, -1, Color.black, target);


            SetObservation(labelFilePath);

            //Set hypothesis(current transform).
            double[] particle = new double[6];
            particle[0] = target.transform.position.x;
            particle[1] = target.transform.position.y;
            particle[2] = target.transform.position.z;
            particle[3] = target.transform.rotation.eulerAngles.x;
            particle[4] = target.transform.rotation.eulerAngles.y;
            particle[5] = target.transform.rotation.eulerAngles.z;


            double error = Evaluate_hypothesis(MarshalJuggedToC(particle), 6);
            Debug.Log("Current cost : " + error.ToString());

            ////debuging용 diffTexture
            //Utils.WriteTexture(diffTexture, "diffTexture.png", TextureFormat.RGBA32);

            //destroy camera
            UnityEngine.Object.DestroyImmediate(global_captureCamera);

            return error;
        }
        IntPtr Evaluate_hypotheses(IntPtr unmanaged_swarm, int swarmSize, int dim)
        {
            float startTime = Time.realtimeSinceStartup;

            double[] swarm = new double[swarmSize * dim];
            Marshal.Copy(unmanaged_swarm, swarm, 0, swarm.Length);

            double[] errors = new double[swarmSize];
            Texture2DArray hypotheses_Textures = GenerateTexture2DArrayfromHypotheses(global_captureCamera, swarm, swarmSize, dim, hypothesisType);
            //RenderTexture hypotheses_Textures = GenerateTexture2DArrayfromHypotheses2(global_captureCamera, swarm, swarmSize, dim);
            //Debug.Log("Time to set texture2darray " + (Time.realtimeSinceStartup - startTime) + " sec");
            //WriteTexture(hypotheses_Textures, "hypotheses_Textures.png");

            //startTime = Time.realtimeSinceStartup;
            //for (int k = 0; k < 100; k++)
            {
                //startTime = Time.realtimeSinceStartup;
                computeShader_diff.SetTexture(kernel_diff, "hypotheses", hypotheses_Textures);
                computeShader_diff.Dispatch(kernel_diff, gridDimX, gridDimY, gridDimZ);
                buffer_diffMap.GetData(data_diffMap);
                UnityEngine.Object.DestroyImmediate(hypotheses_Textures); // 테스트안해봄 2020130.
                                                                          //Debug.Log("Time to compute diff : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

                //int sum = SumEvalMapTest(data_diffMap, equirectangularWidth, equirectangularWidth, gridDimZ);
                //Debug.Log("eval1 :" + sum.ToString());

                //reduction 1.
                //startTime = Time.realtimeSinceStartup;
                Sum_reduction(computeShader_sum, gridDimX, gridDimX, gridDimZ, buffer_diffMap, buffer_evalMap_after_reduction1);
                buffer_evalMap_after_reduction1.GetData(data_evalMap_after_reduction1);

                //Debug.Log("Time to compute reduction1 : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

                //int sum2 = SumEvalMapTest(data_evalMap_after_reduction1, gridDimX, gridDimX, gridDimZ);
                //Debug.Log("eval2 :" + sum2.ToString());

                //reduction 2.
                //startTime = Time.realtimeSinceStartup;
                Sum_reduction(computeShader_sum2, 1, 1, gridDimZ, buffer_evalMap_after_reduction1, buffer_eval);
                buffer_eval.GetData(data_eval);

                // Debug.Log("Time to compute reduction2 : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

                //int sum3 = SumEvalMapTest(data_eval, 1, 1, gridDimZ);
                //Debug.Log("eval3 :" + sum3.ToString());

                for (int i = 0; i < swarmSize; i++)
                {
                    errors[i] = data_eval[i] / (double)(equirectangularWidth * equirectangularHeight);
                }
            }
            //Debug.Log("Time to total computebuffer : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

            return MarshalJuggedToC(errors);
        }

        double Evaluate_hypothesis(IntPtr unmanaged_particle, int dim)
        {
            double[] particle = new double[dim];
            Marshal.Copy(unmanaged_particle, particle, 0, particle.Length);

            ///
            //double[] errors = new double[swarmSize];
            double errors;
            RenderTexture hypothsis = GenerateTexturefromHypothis(global_captureCamera, particle, dim, hypothesisType);
            //RenderTexture hypotheses_Textures = GenerateTexture2DArrayfromHypotheses2(global_captureCamera, swarm, swarmSize, dim);
            //Debug.Log("Time to set texture2darray " + (Time.realtimeSinceStartup - startTime) + " sec");
            //EquirectangularGenerator.WriteTexture(hypothsis, "hypothesis.png");



            //startTime = Time.realtimeSinceStartup;
            computeShader_diff.SetTexture(kernel_diff, "hypothesis", hypothsis);
            computeShader_diff.Dispatch(kernel_diff, gridDimX, gridDimY, gridDimZ);
            buffer_diffMap.GetData(data_diffMap);
            //hypothsis.Release(); //important.
            //UnityEngine.Object.DestroyImmediate(hypothsis);
            hypothsis.Release();
            hypothsis.DiscardContents();

            //Debug.Log("Time to compute diff : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");



            //int sum = SumEvalMapTest(data_diffMap, equirectangularWidth, equirectangularWidth, gridDimZ);
            //Debug.Log("eval1 :" + sum.ToString());

            //reduction 1.
            //startTime = Time.realtimeSinceStartup;
            Sum_reduction(computeShader_sum, gridDimX, gridDimX, gridDimZ, buffer_diffMap, buffer_evalMap_after_reduction1);
            buffer_evalMap_after_reduction1.GetData(data_evalMap_after_reduction1);
            //Debug.Log("Time to compute reduction1 : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

            //int sum2 = SumEvalMapTest(data_evalMap_after_reduction1, gridDimX, gridDimX, gridDimZ);
            //Debug.Log("eval2 :" + sum2.ToString());

            //reduction 2.
            //startTime = Time.realtimeSinceStartup;
            Sum_reduction(computeShader_sum2, 1, 1, gridDimZ, buffer_evalMap_after_reduction1, buffer_eval);
            buffer_eval.GetData(data_eval);
            //Debug.Log("Time to compute reduction1 : " + (Time.realtimeSinceStartup - startTime).ToString() + " sec");

            //int sum3 = SumEvalMapTest(data_eval, 1, 1, gridDimZ);
            //Debug.Log("eval3 :" + sum3.ToString());

            errors = data_eval[0] / (double)(equirectangularWidth * equirectangularHeight);

            return errors;
        }


        void InitComputeShaderandBuffers(int equirectangularWidth, int equirectangularHeight)
        {
            //set grid dimension.
            gridDimX = equirectangularWidth / groupDimX;
            gridDimY = equirectangularHeight / groupDimY;
            gridDimZ = 1;

            //compute shader for diff.
            if (hypothesisType == HypothesisType.OutlinePixels)
                LoadComputeShader(ComputeShaderName.Diffrentiate_Outline, ref computeShader_diff, ref kernel_diff);

            else if (hypothesisType == HypothesisType.AllPixels)
                LoadComputeShader(ComputeShaderName.Diffrentiate, ref computeShader_diff, ref kernel_diff);

            computeShader_diff.SetInt("gridDimX", gridDimX);
            computeShader_diff.SetInt("gridDimY", gridDimY);
            computeShader_diff.SetInt("gridDimZ", gridDimZ);


            buffer_diffMap = new ComputeBuffer(equirectangularWidth * equirectangularWidth * gridDimZ, sizeof(int), ComputeBufferType.Default);
            data_diffMap = new int[equirectangularWidth * equirectangularWidth * gridDimZ];
            buffer_diffMap.SetData(data_diffMap);
            computeShader_diff.SetBuffer(kernel_diff, "buffer_diffMap", buffer_diffMap);

            //compute shader for sum.
            LoadComputeShader(ComputeShaderName.SumXYPlane, ref computeShader_sum, ref kernel_sum);
            computeShader_sum.SetInt("gridDimX", gridDimX);
            computeShader_sum.SetInt("gridDimY", gridDimX); // note. it must be gridDimX.
            computeShader_sum.SetInt("gridDimZ", gridDimZ);

            buffer_evalMap_after_reduction1 = new ComputeBuffer(gridDimX * gridDimX * gridDimZ, sizeof(int), ComputeBufferType.Default);
            data_evalMap_after_reduction1 = new int[gridDimX * gridDimX * gridDimZ];
            buffer_evalMap_after_reduction1.SetData(data_evalMap_after_reduction1);
            computeShader_sum.SetBuffer(kernel_sum, "output", buffer_evalMap_after_reduction1);

            //compute shader for sum.


            //kernel_sum2 = computeShader_sum2.FindKernel("SumXYPlane256");
            if (equirectangularWidth == 256)
                LoadComputeShader(ComputeShaderName.SumXYPlaneFinal256, ref computeShader_sum2, ref kernel_sum2);

            else if (equirectangularWidth == 512)
                LoadComputeShader(ComputeShaderName.SumXYPlaneFinal512, ref computeShader_sum2, ref kernel_sum2);

            else if (equirectangularWidth == 1024)
                LoadComputeShader(ComputeShaderName.SumXYPlaneFinal1024, ref computeShader_sum2, ref kernel_sum2);

            computeShader_sum2.SetInt("gridDimX", 1);
            computeShader_sum2.SetInt("gridDimY", 1);
            computeShader_sum2.SetInt("gridDimZ", gridDimZ);

            buffer_eval = new ComputeBuffer(gridDimZ, sizeof(int), ComputeBufferType.Default);
            data_eval = new int[gridDimZ];
            buffer_eval.SetData(data_eval);
            computeShader_sum2.SetBuffer(kernel_sum2, "output", buffer_eval);


            ////debuging용 diffTexture
            diffTexture = new RenderTexture(equirectangularWidth, equirectangularHeight, 0);
            diffTexture.enableRandomWrite = true;
            diffTexture.Create();
            computeShader_diff.SetTexture(kernel_diff, "diffTexture", diffTexture);
        }

        void LoadComputeShader(ComputeShaderName name, ref ComputeShader computeShader, ref int kernel)
        {
            string filename = name.ToString();
            string[] guids = AssetDatabase.FindAssets(filename);

            if (guids.Length == 0)
                throw new FileNotFoundException(filename);
            string assetPath = AssetDatabase.GUIDToAssetPath(guids[0]);

            computeShader = (ComputeShader)AssetDatabase.LoadAssetAtPath(assetPath, typeof(ComputeShader));

            kernel = computeShader.FindKernel(filename);
        }

        void InitPSO(int swarmSize, int iteration, double threshold, bool halt_iteration, bool halt_threshold, double[] initialPose)
        {

            CONFIG config = new CONFIG()
            {
                DIM = 6,
                NUM_PARTICLE = swarmSize,
                ITERATION = iteration,
                STRATEGY_SOCIAL = (int)STRATEGY_SOCIAL.STRATEGY_GLOBAL,
                STRATEGY_WEIGHT = (int)STRATEGE_WEIGHT.STRATEGY_W_LIN_DEC,
                COUNT_NO_IMPROVEMENT = 10,

                ERROR_THRESHOLD = threshold,

                HALT_THRESHOLD = Convert.ToByte(halt_iteration),
                HALT_ITERATION = Convert.ToByte(halt_threshold),
                HALT_NO_IMPROVEMENT = Convert.ToByte(false),

                INERTIA_BOUND_LOW = 0.3,
                INERTIA_BOUND_HIGH = 0.9,

                C1 = 1.496,
                C2 = 1.496,

                STD_INITIAL_POSE = 0.05 // 0.05 is standard.
            };


            RANGE[] arrBounds = new RANGE[config.DIM];
            //double[] arrInitPoint = new double[config.DIM];
            //initialPose.CopyTo(arrInitPoint, 0);
            //ConvertToArray(initialPose);

            //translation.
            for (int i = 0; i < 3; i++)
            {
                arrBounds[i].lower = initialPose[i] - 8;
                arrBounds[i].upper = initialPose[i] + 8;
            }
            //arrBounds[1].lower = arrInitPoint[1] - 2;
            //arrBounds[1].upper = arrInitPoint[1] + 2;

            //rotation.
            for (int i = 3; i < 6; i++)
            {
                arrBounds[i].lower = initialPose[i] - 5;
                arrBounds[i].upper = initialPose[i] + 5;
            }


            //ParticleSwarmOptimizationWarpper.Instance.InitSwarm_Type2(config, arrInitPoint, arrBounds, new Callback_Type2(Evaluate_hypotheses));
            ParticleSwarmOptimizationWarpper.Instance.InitSwarm(config, initialPose, arrBounds, new Callback(Evaluate_hypothesis));
        }

        void SetObservation(string labelfilepath)
        {

            if (hypothesisType == HypothesisType.AllPixels)
                //observation = Utils.LoadTextureUsingAssetDataBase(assetPath_label, equirectangularWidth);//EquirectangularGenerator.GetEquirectangularTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera);
                observation = TextureUtils.LoadTexture(labelfilepath, true, equirectangularWidth);//EquirectangularGenerator.GetEquirectangularTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera);

            else //(hypothesisType == HypothesisType.OutlinePixels)
                 //observation = OutlineDetectionWarpper.LoadTexture(labelfilepath, true, equirectangularWidth);//EquirectangularGenerator.GetEquirectangularTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera);
                observation = OutlineDetectionWarpper.GetOutlineTexture(labelfilepath, true, equirectangularWidth, Color.white, 2);

            //EquirectangularGenerator.WriteTexture(observation, "observation.png");
            computeShader_diff.SetTexture(kernel_diff, "observation", observation);
            //Debug.Log("Time to load & set observation : " + (Time.realtimeSinceStartup - startTime) + " sec");

        }

        void Sum_reduction(ComputeShader sumShader, int gridDimX, int gridDimY, int gridDimZ, ComputeBuffer inputBuffer, ComputeBuffer outputBuffer)
        {
            sumShader.SetBuffer(kernel_sum, "inputMap", inputBuffer);
            sumShader.SetBuffer(kernel_sum, "outputMap", outputBuffer);
            sumShader.Dispatch(kernel_sum, gridDimX, gridDimY, gridDimZ);

        }

        Texture2DArray GenerateTexture2DArrayfromHypotheses(GameObject captureCamera, double[] swarm, int swarmSize, int dim, HypothesisType hypothesisType)
        {
            float startTime = Time.realtimeSinceStartup;

            Texture2DArray texture2DArray = new Texture2DArray(equirectangularWidth, equirectangularHeight, swarmSize, TextureFormat.RGBA32, false);

            for (int i = 0; i < swarmSize; i++)
            {
                //Get hypothesis.
                captureCamera.transform.position = new Vector3((float)swarm[i * dim + 0], (float)swarm[i * dim + 1], (float)swarm[i * dim + 2]);
                captureCamera.transform.rotation = Quaternion.Euler((float)swarm[i * dim + 3], (float)swarm[i * dim + 4], (float)swarm[i * dim + 5]);

                RenderTexture hypothesis;

                if (hypothesisType != HypothesisType.OutlinePixels)
                    hypothesis = EquirectangularGenerator.GetEquirectangularUnlitColorTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera);
                else
                    hypothesis = EquirectangularGenerator.GetEquirectangularContourTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera, 2, Color.red);

                //WriteTexture(hypothesis, "hypothesis" + i.ToString() +".png");

                Texture2D hypothesis_tex2d = new Texture2D(equirectangularWidth, equirectangularHeight, TextureFormat.RGBA32, false);

                /*
                //copy texture using computeshader.
                computeShader_copyTexture.SetTexture(kernel_copyTexture, "srceTexture", hypothesis);
                computeShader_copyTexture.SetTexture(kernel_copyTexture, "destTexture", hypothesis_tex2d);
                computeShader_copyTexture.Dispatch(kernel_copyTexture, equirectangularWidth / 32, equirectangularHeight / 32, 1);
                */

                hypothesis_tex2d.ReadPixels(new Rect(0, 0, equirectangularWidth, equirectangularHeight), 0, 0, false);
                //Graphics.CopyTexture(hypothesis, hypothesis_tex2d);
                hypothesis_tex2d.Apply();
                texture2DArray.SetPixels(hypothesis_tex2d.GetPixels(), i, 0);
                UnityEngine.Object.DestroyImmediate(hypothesis_tex2d);// 테스트안해봄 2020130.
                hypothesis.Release();// 테스트안해봄 2020130.

            }
            texture2DArray.Apply();
            return texture2DArray;
        }

        RenderTexture GenerateTexturefromHypothis(GameObject captureCamera, double[] particle, int dim, HypothesisType hypothesisType)
        {
            //float startTime = Time.realtimeSinceStartup;
            //Get hypothesis.
            captureCamera.transform.position = new Vector3((float)particle[0], (float)particle[1], (float)particle[2]);
            captureCamera.transform.rotation = Quaternion.Euler((float)particle[3], (float)particle[4], (float)particle[5]);

            RenderTexture hypothesis;

            if (hypothesisType == HypothesisType.AllPixels)
                hypothesis = EquirectangularGenerator.GetEquirectangularUnlitColorTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera);
            else
                hypothesis = EquirectangularGenerator.GetEquirectangularContourTexture(equirectangularWidth, equirectangularHeight, cubemapWidth, captureCamera, 2, Color.white);

            //EquirectangularGenerator.WriteTexture(hypothesis, "hypothesis" + ".png");

            return hypothesis;
        }

        double[] ConvertToArray(Transform transform)
        {
            double[] result = new double[6];

            result[0] = transform.position.x;
            result[1] = transform.position.y;
            result[2] = transform.position.z;

            result[3] = transform.rotation.eulerAngles.x;
            result[4] = transform.rotation.eulerAngles.y;
            result[5] = transform.rotation.eulerAngles.z;

            return result;
        }

        int SumEvalMapTest(int[] evalmap, int width, int height, int depth)
        {

            int sum = 0;

            for (int z = 0; z < depth; z++)
            {
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        sum += evalmap[z * width * height + y * width + x];
                    }
                }
            }

            return sum;

        }

        static IntPtr MarshalJuggedToC(double[] managedArray)
        {
            // Initialize unmanaged memory to hold the array.
            int size = Marshal.SizeOf(managedArray[0]) * managedArray.Length;
            IntPtr pnt = Marshal.AllocCoTaskMem(size);

            // Copy the array to unmanaged memory.
            Marshal.Copy(managedArray, 0, pnt, managedArray.Length);

            return pnt;
        }

    }

}
