﻿using System;
using System.Runtime.InteropServices;

namespace PSO
{
    public enum STRATEGY_SOCIAL
    {
        /*
		Strategy constants for Social Factor Influence
	    */
        STRATEGY_GLOBAL = 0,
        STRATEGY_KNN = 1,
    }

    public enum STRATEGE_WEIGHT
    {
        /*
        Constants for Inertia Weight Configuration 
        */
        STRATEGY_W_CONST = 0,
        STRATEGY_W_LIN_DEC = 1,
    };
    public struct CONFIG
    {
        public int DIM;
        public int NUM_PARTICLE;
        public int ITERATION;
        public int STRATEGY_SOCIAL;
        public int STRATEGY_WEIGHT;
        public int COUNT_NO_IMPROVEMENT;

        public double ERROR_THRESHOLD;

        public byte HALT_THRESHOLD;
        public byte HALT_ITERATION;
        public byte HALT_NO_IMPROVEMENT;

        public double INERTIA_BOUND_LOW;
        public double INERTIA_BOUND_HIGH;

        public double C1;
        public double C2;

        public double STD_INITIAL_POSE;
    }
    public struct RANGE
    {
        public double lower;
        public double upper;
    }

    public struct RESULT
    {
        public IntPtr gBest;
        public double gBest_Err;
    }

    //public unsafe delegate double* Callback(double** arr, int col, int row);
    //public unsafe delegate double* Callback1D(double* arr, int col, int row);

    public delegate double Callback(IntPtr particle, int dim);
    public delegate IntPtr Callback_Type2(IntPtr swarm, int swarmSize, int dim);

    public class ParticleSwarmOptimizationWarpper
    {
        static Callback myCallback;
        static Callback_Type2 myCallback_type2;
        static ParticleSwarmOptimizationWarpper _instance;
        public static ParticleSwarmOptimizationWarpper Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new ParticleSwarmOptimizationWarpper();

                return _instance;
            }
        }

        const string DLL_NAME = "./Assets/Plugins/libPSO.dll";

        [DllImport(DLL_NAME)]
        static extern int _CreateSwarm(CONFIG config, double[] initialPoint, RANGE[] bounds, Callback func);

        [DllImport(DLL_NAME)]
        static extern int _CreateSwarm_Type2(CONFIG config, double[] initialPoint, RANGE[] bounds, Callback_Type2 func);

        [DllImport(DLL_NAME)]
        static extern RESULT _MoveOneStep(int iter);

        [DllImport(DLL_NAME)]
        static extern RESULT _FindFood();

        [DllImport(DLL_NAME)]
        static extern int _DestroySwarm();

        //[DllImport(DLL_NAME)]
        //static extern IntPtr Test(ObjectiveFunction func);

        public int InitSwarm(CONFIG config, double[] initialPoint, RANGE[] bounds, Callback func)
        {
            myCallback = func;
            return _CreateSwarm(config, initialPoint, bounds, myCallback); //must deliver static member 'myCallback' not 'func'.
        }

        public int InitSwarm(CONFIG config, double[] initialPoint, RANGE[] bounds, Callback_Type2 func)
        {
            myCallback_type2 = func;
            return _CreateSwarm_Type2(config, initialPoint, bounds, myCallback_type2);
        }

        public RESULT MoveOneStep(int iter)
        {
            return _MoveOneStep(iter);
        }

        public RESULT Optimzation(CONFIG config)
        {
            int iter = 0;
            RESULT result = new RESULT();

            while (true)
            {
                result = _MoveOneStep(iter++);

                //halt if iteration count is max.
                if (Convert.ToBoolean(config.HALT_ITERATION) && iter == config.ITERATION)
                    break;

                //halt if error is low than threshold.
                if (Convert.ToBoolean(config.HALT_THRESHOLD) && (result.gBest_Err < config.ERROR_THRESHOLD))
                    break;

                //    //halt if no improvemnt.
                //    //if (){}

                //    //printf("\n");
            }
            return result;
        }

        public int DestroySwarm()
        {
            return _DestroySwarm();
        }

        //public void test(ObjectiveFunction func)
        //{
        //    Test(func);
        //}

    }

}
