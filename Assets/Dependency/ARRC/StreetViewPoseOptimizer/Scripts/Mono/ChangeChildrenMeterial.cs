﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeChildrenMeterial : MonoBehaviour
{
    public Material material;
    MeshRenderer[] meshRenderers;
    public bool keep_texture = true;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void SetSharedMeterial(Material newMaterial)
    {
        meshRenderers = transform.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer renderer in meshRenderers)
        {
            if(keep_texture)
                newMaterial.mainTexture = renderer.sharedMaterial.mainTexture;

            renderer.sharedMaterial = newMaterial;
        }
            
    }

}
