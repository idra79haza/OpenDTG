﻿using UnityEditor;
using UnityEngine;
using System.IO;
using ARRC.DigitalTwinGenerator;
using ARRC.ARRCTexture;

enum TestType { Original, UnlitColor, PseudoDepth, TurboColor, Contour }

class EquirectangularGeneratorTestWizard : ScriptableWizard
{
    public Transform renderPose = null;
    public TestType testType = TestType.Original;
    public Color backgroundColor = new Color(1, 1, 1, 1);

    public int equirectangularWidth = 2048;
    public int equirectangularHeight = 1024;
    
    public LayerMask cameraLayerMask = -1;
    //public LayerMask cameraLayerMask = 1 << 8;
    public float cameraNearPlane = 0.3f;
    public float cameraFarPlane = 256;
    public string outputPath = "equirectangularTexture.png";

    [MenuItem("ARRC/StreetView Optimizer/Equirectangular Generator Test")]
    static void CreateWizard()
    {
        DisplayWizard<EquirectangularGeneratorTestWizard>("Test Equirectangular Generation", "Close", "Create");
    }
    void OnWizardOtherButton()
    {
        if (renderPose == null)
            return;

        // Create temporary camera for rendering, Confirm details in this function.
        GameObject captureCamera = EquirectangularGenerator.CreateCameraForRenderTexture(cameraLayerMask, cameraNearPlane, cameraFarPlane, -1, backgroundColor, renderPose);

        RenderTexture rtex_equi;

        float startTime = Time.realtimeSinceStartup;

        switch (testType)
        {
            default:
                rtex_equi = EquirectangularGenerator.GetEquirectangularTexture(equirectangularWidth, equirectangularHeight, 512, captureCamera.gameObject);
                break;

            case TestType.PseudoDepth:
                rtex_equi = EquirectangularGenerator.GetEquirectangularPsudoDepthTexture(equirectangularWidth, equirectangularHeight, 512, captureCamera);
                break;

            case TestType.TurboColor:
                rtex_equi = EquirectangularGenerator.GetEquirectangularTurboColorTexture(equirectangularWidth, equirectangularHeight, 512, captureCamera);
                break;

            case TestType.UnlitColor:
                rtex_equi = EquirectangularGenerator.GetEquirectangularUnlitColorTexture(equirectangularWidth, equirectangularHeight, 512, captureCamera);
                break;

            case TestType.Contour:
                rtex_equi = EquirectangularGenerator.GetEquirectangularContourTexture(equirectangularWidth, equirectangularHeight, 512, captureCamera.gameObject, 2, Color.red);
                break;
        }
        
        Debug.Log("Time to render to texture: " + (Time.realtimeSinceStartup - startTime) + " sec");
        TextureUtils.WriteTexture(rtex_equi, outputPath, TextureFormat.ARGB32);
        AssetDatabase.Refresh();
        rtex_equi.Release(); //Neccesary.

        DestroyImmediate(captureCamera);

        
    }
    private void OnSelectionChange()
    {
        if (Selection.activeGameObject)
        {
            renderPose = Selection.activeGameObject.transform;
            Repaint();
        }

    }
 
}