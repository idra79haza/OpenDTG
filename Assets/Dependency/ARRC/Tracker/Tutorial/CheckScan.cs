﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckScan: MonoBehaviour
{
    public GameObject ScanGuide_Complete;

    private void Start()
    {
    }

    void CompleteScan()
    {

        ScanGuide_Complete.SetActive(true);
        gameObject.SetActive(false);
    }

}
