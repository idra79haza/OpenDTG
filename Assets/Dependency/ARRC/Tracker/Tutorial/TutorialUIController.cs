﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject stepsRoot;

    private List<GameObject> steps;

    private int maxCount;
    private int currentIndex;

    private void Awake()
    {
        steps = new List<GameObject>();
        maxCount = stepsRoot.transform.childCount;
        foreach (Transform child in stepsRoot.transform)
        {
            steps.Add(child.gameObject);
        }
    }

    private void Start()
    {
    }

    private void OnEnable()
    {
        currentIndex = 0;

        foreach(GameObject step in steps)
        {
            step.SetActive(false);
        }
        steps[currentIndex].SetActive(true);
    }

    public void NextStep()
    {
        steps[currentIndex].SetActive(false);
        currentIndex++;
        if(currentIndex < maxCount)
        {
            steps[currentIndex].SetActive(true);
        }
    }
}
