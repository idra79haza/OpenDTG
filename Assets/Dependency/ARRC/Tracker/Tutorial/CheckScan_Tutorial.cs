﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckScan_Tutorial : MonoBehaviour
{
    public TutorialUIController tutorialRoot;

    private void Start()
    {
        tutorialRoot = GameObject.Find("TutorialRoot").GetComponent<TutorialUIController>();
    }

    void CompleteScan()
    {
        tutorialRoot.NextStep();
    }

}
