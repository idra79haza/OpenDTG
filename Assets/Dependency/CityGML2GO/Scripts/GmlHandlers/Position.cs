﻿using ARRC.DigitalTwinGenerator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using UnityEngine;

namespace CityGML2GO.GmlHandlers
{
    public class PositionHandler
    {
        static string CleanPos(string pos)
        {
            return pos.Replace("\r", " ").Replace("\n", " ").Replace("\t", " ");
        }

        public static Vector3 GetPos(XmlReader reader, CoordinateInfo coordinateInfo)
        {
            if (string.IsNullOrEmpty(reader.Value))
            {
                reader.Read();
            }
            var parts = CleanPos(reader.Value).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);

            double parseX = double.Parse(parts[0]);
            double parseY = double.Parse(parts[2]);
            double parseZ = double.Parse(parts[1]);

            coordinateInfo.ConvertXY(parseX, parseZ, out double convertedX, out double convertedZ);

            Vector3 pos = new Vector3((float)(convertedX - coordinateInfo.targetBoundary.minX), (float)parseY, (float)(convertedZ - coordinateInfo.targetBoundary.minY));

            return pos;

        }

        public static List<Vector3> GetPosList(XmlReader reader, CoordinateInfo coordinateInfo)
        {
            var retVal = new List<Vector3>();
            var parts = CleanPos(reader.Value).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length < 2)
            {
                parts = CleanPos(reader.ReadInnerXml()).Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
            }
            if (parts.Length % 3 != 0)
            {
                Debug.LogWarning("Error in posList (Count % 3 != 0)");
                Debug.Log(((IXmlLineInfo)reader).LineNumber);
                return null;
            }

            for (int i = 0; i < parts.Length; i += 3)
            {
                double parseX = double.Parse(parts[i + 0]);
                double parseY = double.Parse(parts[i + 2]);
                double parseZ = double.Parse(parts[i + 1]);

                coordinateInfo.ConvertXY(parseX, parseZ, out double convertedX, out double convertedZ);

                Vector3 pos = new Vector3((float)(convertedX - coordinateInfo.targetBoundary.minX), (float)parseY, (float)(convertedZ - coordinateInfo.targetBoundary.minY));

                retVal.Add(pos);

            }

            return retVal;
        }

    }
}
