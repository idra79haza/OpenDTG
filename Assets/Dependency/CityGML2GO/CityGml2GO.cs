﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Proj4Net;
using System;
using System.Linq;
using ARRC.DigitalTwinGenerator;

namespace CityGML2GO
{
    public class CityGml2GO
    {
        public CoordinateInfo coordinateInfo;
        //boundary

        // Use this for initialization
        //public Vector3 Translate = Vector3.zero;
        public bool ShowDebug;
        public float UpdateRate;
        public bool ShowCurves;
        public bool Semantics;
        public float CurveThickness;
        public GameObject LineRendererPrefab;
        public bool GenerateColliders;
        public List<string> SemanticSurfaces = new List<string> { "GroundSurface", "WallSurface", "RoofSurface", "ClosureSurface", "CeilingSurface", "InteriorWallSurface", "FloorSurface", "OuterCeilingSurface", "OuterFloorSurface", "Door", "Window" };
        public Material DefaultMaterial;

        public double originX, originY;

        public double minX, minY, maxX, maxY;
        public string title;
        public bool useSharedMemory;

        public CityGml2GO(string title, bool useSharedMemory, CoordinateInfo coordinateInfo)
        {
            this.title = title;
            this.coordinateInfo = coordinateInfo;
            this.useSharedMemory = useSharedMemory;

            DefaultMaterial = new Material(Shader.Find("Standard"));
            DefaultMaterial.color = new Color(255, 0, 0, 0.125f);
        }

    }
}
